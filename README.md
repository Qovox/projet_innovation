# Chroma.Library

Library introducing new interactions with Philips Hue lights. This library use bidirectionnal communications with the Philips Hue bridge to get and set information of a light.

## Project Dependencies

* [Q42.HueApi](https://github.com/Q42/Q42.HueApi) : Open source library for communication with the Philips Hue bridge.

## How to use

Basic commands

### Bridge

First of all, you need to establish a connection with the Philips Hue bridge.

```cs
PhilipsHueClient client = new PhilipsHueClient();
await client.InitializeAsync();
```

### Interact with the lights

The class _PhilipsHueClient_ contains all the methods to interact with the lights.

You might need to know if you are currently connected to the Philips Hue bridge.

```cs
await client.CheckConnectionAsync();
```

Then you can control your lights. For each of these methods you can set the transition time (multiple of 10ms).

```cs
/// Turn On one or all lights
client.TurnOnLights(lights: 1);  // only light 1
client.TurnOnLights(100, new int[] {1,2}) // turn on the lights 1 and 2 with a transition time of 100 ms
client.TurnOnLights();   // all lights

/// Declare a color with RGBColor
RGBColor rgbColor = new RGBColor(4, 239, 169);

// Set this color to the light with id 1
client.SetLightsColor(rgbColor, lights: 1);

// Set this color to all lights
client.SetLightsColor(rgbColor);

// Set the brightness to light 2
client.SetLightsBrightness(0.1, lights: 2);

// Turn Off one or all lights
client.TurnOffLights(lights: 1); // only light 1
client.TurnOffLights();  // all lights
```

The main innovation of the library is that you can get the actual color of a light or other states.

```cs
// Three different methods to get the actual color of a light
RGBColor colorRGB = client.GetLightRGBColor(1); // color in RGB
HSVColor colorHSV = client.GetLightHSVColor(1); // color in HSV
string colorHEX = client.GetLightHexColor(1); // color in hexadecimal

float bri = client.GetLightBrightness(1); // light 1 brightness
bool isOn = client.GetLightState(2); // light 2 state (true : light is on, false otherwise)
```

### Philips Hue Animation

The class _PhilipsHueAnimation_ allows you to create your own light animations.

```cs
PhilipsHueAnimation animation = new PhilipsHueAnimation();

// Create an animation for the light 1
animation
    .SetColor(ChromaColors.Blue, lights: 1)
    .SetBrightness(0.5f, 0, lights: 1)
    .TurnOn(1600, lights: 1);

// Play the animation with the PhilipsHueClient three times
client.PlayAnimation(animation, 3);
```

### Subscribe to lights changes

If you want to be informed of a light change, you can use our system of subscribing.

```cs
// you have to declare a method that can be raised by the event. See below.
var eventRaised = new client.LightChangeEventHandler(OnEventRaisedFeature);

// Subscribe to event
client.StartCacheAutoUpdate();
client.LightChangeEvent += eventRaised;

/***
Here you method eventRaised can be raised anytime a light's state changes.
***/

// Unsubscribe from event
client.StopCacheAutoUpdate();
client.LightChangeEvent -= eventRaised;
```

You have to declare a local method that will be raised by the event.

```cs
private void OnEventRaisedFeature(client.LightChangeEventArgs e)
{
    Console.WriteLine("Light id event raised: " + e._lightId);
    Console.WriteLine("Light id colorhex update: " + e._HexColor + "\n");
}
```

The object LightChangeEventArgs contains all usefull informations about the light's state change :

* int **_lightId**, ID of the light.
* bool **_On**, true of the light is On, false otherwise.
* RGBcolor **_RGBColor**, the new RGBColor of the light.
* HSVColor **_HSVColor**, the new HSVColor of the light.
* string **_HexColor**, the new hexadecimal color of the light.
* float **_Brightness**, the new level of brightness of the light (between 0 and 1).

By default the library will check every 100ms if there are any light changes. This can be customized at the initalization of the client.

```cs
await client.InitializeAsync(50); // timer will be set at 50ms.
```

### Gaming Animation with Philips Hue

Check what has been done concerning the [gaming part with the Philips Hue Lights](Gaming.md).

## How to install

Download the library DLL via [Dropbox](https://www.dropbox.com/s/8uqny8so9x0t6uh/Chroma.Library.dll?dl=0) and import it into your project.

## Documentation

[Chroma library official documentation](http://sticmac.fr/chromalib/index.html)

## Related Projects

* [chroma_game](https://mjollnir.unice.fr/bitbucket/projects/PNSC/repos/chroma_game/browse) : Game designed to be used with the Philips Hue lights and the Chroma library
* [prototypes](https://mjollnir.unice.fr/bitbucket/projects/PNSC/repos/prototypes/browse) : Set of games/prototypes to show the utility of using Philips Hue lights in video games