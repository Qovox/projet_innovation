var class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation =
[
    [ "PhilipsHueAnimation", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#ac1b8365a67b4fbed6d270ea7bbf9eb64", null ],
    [ "AppendAnimation", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a786eb7878665436ef116a3b7a1c9b41a", null ],
    [ "Blink", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a5e56e19d736d356808d7777a84d1c685", null ],
    [ "ColorLoop", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#af18b7e4472a5149c2a1a31dbd40ddfc5", null ],
    [ "SetBrightness", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#aee36f4b66e198d06d895624984e1de2d", null ],
    [ "SetColor", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a42580c96dc43014b6eec389193b43809", null ],
    [ "SetColor", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a87cdaf4ce24f8171819af21226ed4d7c", null ],
    [ "TurnOff", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a1ff84904a9d86daac3c44c1477b8d089", null ],
    [ "TurnOn", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#ae8a9beb7eacad2bf10311617bfc75ccf", null ],
    [ "Wait", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#adc9e0253307768acd471cf53fc7ebd9e", null ],
    [ "Frames", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a99e0387fdf3be6c5c6ff1b9bccb4a7bf", null ],
    [ "IsModified", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#ad0190d8e4e8cc681e070105ff9223ce6", null ],
    [ "IsStopped", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a8490d0b2db965caa2033c3c21549f126", null ],
    [ "Lights", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a4375a6f021ce7874ca34018c3bc08cd3", null ],
    [ "ModifiedLights", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#ac7b16b3c3c13ec2bd269db1215ba11fb", null ],
    [ "WaitAfterAnimation", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a741359813340d712a9b984880f681a55", null ]
];