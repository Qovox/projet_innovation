var namespace_chroma_1_1_library =
[
    [ "Color", "namespace_chroma_1_1_library_1_1_color.html", "namespace_chroma_1_1_library_1_1_color" ],
    [ "ConsoleSample", "namespace_chroma_1_1_library_1_1_console_sample.html", "namespace_chroma_1_1_library_1_1_console_sample" ],
    [ "Gaming", "namespace_chroma_1_1_library_1_1_gaming.html", "namespace_chroma_1_1_library_1_1_gaming" ],
    [ "Interfaces", "namespace_chroma_1_1_library_1_1_interfaces.html", "namespace_chroma_1_1_library_1_1_interfaces" ],
    [ "PhilipsHue", "namespace_chroma_1_1_library_1_1_philips_hue.html", "namespace_chroma_1_1_library_1_1_philips_hue" ],
    [ "WrappedObjects", "namespace_chroma_1_1_library_1_1_wrapped_objects.html", "namespace_chroma_1_1_library_1_1_wrapped_objects" ]
];