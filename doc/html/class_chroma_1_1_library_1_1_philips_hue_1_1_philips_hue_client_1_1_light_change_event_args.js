var class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args =
[
    [ "LightChangeEventArgs", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#a4f05f97bf2dd6393504350bf800c8b87", null ],
    [ "_Brightness", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#a60a646457e193f9bac55c5284fe7c0bb", null ],
    [ "_HexColor", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#aae3d78924b76cc37ad38071bff602e5b", null ],
    [ "_HSVColor", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#afa8a9e3d7a9f085e4675cf3bccc445ed", null ],
    [ "_lightId", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#a917789156ddd4fbbcd0da948e77475a3", null ],
    [ "_On", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#ab88a65693b52a7cd0dbc7a73b33478c3", null ],
    [ "_RGBColor", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#a6acac3a3e661ac0e4fa2172db9457aaf", null ]
];