var searchData=
[
  ['tohex',['ToHex',['../class_chroma_1_1_library_1_1_color_1_1_h_s_v_color.html#a7235831a9f76f043d74208ed88b8d802',1,'Chroma.Library.Color.HSVColor.ToHex()'],['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a7b2b7fc87bf90dd52ac7c59a6eef344b',1,'Chroma.Library.Color.RGBColor.ToHex()']]],
  ['tohsv',['ToHSV',['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#ae522d755c78395ea8c6b32c8e30d99a9',1,'Chroma::Library::Color::RGBColor']]],
  ['torgb',['ToRGB',['../class_chroma_1_1_library_1_1_color_1_1_h_s_v_color.html#a9e2e6f85735dd2dc26b25d6b14eeba27',1,'Chroma::Library::Color::HSVColor']]],
  ['turnoff',['TurnOff',['../interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#a7d42f88c4580985678583f056ab97a81',1,'Chroma.Library.Interfaces.IHueAnimation.TurnOff()'],['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a1ff84904a9d86daac3c44c1477b8d089',1,'Chroma.Library.PhilipsHue.PhilipsHueAnimation.TurnOff()']]],
  ['turnofflights',['TurnOffLights',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#a2623e70c30d32a718eaca1d9791e6c64',1,'Chroma::Library::PhilipsHue::PhilipsHueClient']]],
  ['turnon',['TurnOn',['../interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#ac2ddfee960e42a6109005e3e682f30f3',1,'Chroma.Library.Interfaces.IHueAnimation.TurnOn()'],['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#ae8a9beb7eacad2bf10311617bfc75ccf',1,'Chroma.Library.PhilipsHue.PhilipsHueAnimation.TurnOn()']]],
  ['turnonlights',['TurnOnLights',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#acc0cc91dcdaa3e88c87fc85da25136bc',1,'Chroma::Library::PhilipsHue::PhilipsHueClient']]]
];
