var searchData=
[
  ['ihueanimation',['IHueAnimation',['../interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html',1,'Chroma::Library::Interfaces']]],
  ['ihueclient',['IHueClient',['../interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_client.html',1,'Chroma::Library::Interfaces']]],
  ['initializeasync',['InitializeAsync',['../interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_client.html#ae5c2573d5065e543e88fcdc297bee091',1,'Chroma.Library.Interfaces.IHueClient.InitializeAsync()'],['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#ae68dfe765d6296bbfc371d418b5c9426',1,'Chroma.Library.PhilipsHue.PhilipsHueClient.InitializeAsync()']]],
  ['ipaddress',['IpAddress',['../class_chroma_1_1_library_1_1_wrapped_objects_1_1_located_bridge.html#a46d0dd506b4d06b45142373d16ff840e',1,'Chroma::Library::WrappedObjects::LocatedBridge']]],
  ['isstopped',['IsStopped',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#a8490d0b2db965caa2033c3c21549f126',1,'Chroma::Library::PhilipsHue::PhilipsHueAnimation']]]
];
