var searchData=
[
  ['checkconnectionasync',['CheckConnectionAsync',['../interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_client.html#abba34b1481f9d9a154e6de85d34c28eb',1,'Chroma.Library.Interfaces.IHueClient.CheckConnectionAsync()'],['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#a62cc14c7d212581ccf5e6c86c6195d52',1,'Chroma.Library.PhilipsHue.PhilipsHueClient.CheckConnectionAsync()']]],
  ['colorloop',['ColorLoop',['../interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#a34e3cd81f2eae223c1b4dd4cc1e8d65f',1,'Chroma.Library.Interfaces.IHueAnimation.ColorLoop()'],['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#af18b7e4472a5149c2a1a31dbd40ddfc5',1,'Chroma.Library.PhilipsHue.PhilipsHueAnimation.ColorLoop()']]],
  ['colorlooponlights',['ColorLoopOnLights',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#a97bcd23ff0551de532a57342012cdb54',1,'Chroma::Library::PhilipsHue::PhilipsHueClient']]]
];
