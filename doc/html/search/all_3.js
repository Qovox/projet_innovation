var searchData=
[
  ['checkconnectionasync',['CheckConnectionAsync',['../interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_client.html#abba34b1481f9d9a154e6de85d34c28eb',1,'Chroma.Library.Interfaces.IHueClient.CheckConnectionAsync()'],['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#a62cc14c7d212581ccf5e6c86c6195d52',1,'Chroma.Library.PhilipsHue.PhilipsHueClient.CheckConnectionAsync()']]],
  ['chroma',['Chroma',['../namespace_chroma.html',1,'']]],
  ['color',['Color',['../namespace_chroma_1_1_library_1_1_color.html',1,'Chroma::Library']]],
  ['colorloop',['ColorLoop',['../interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#a34e3cd81f2eae223c1b4dd4cc1e8d65f',1,'Chroma.Library.Interfaces.IHueAnimation.ColorLoop()'],['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#af18b7e4472a5149c2a1a31dbd40ddfc5',1,'Chroma.Library.PhilipsHue.PhilipsHueAnimation.ColorLoop()']]],
  ['colorlooponlights',['ColorLoopOnLights',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#a97bcd23ff0551de532a57342012cdb54',1,'Chroma::Library::PhilipsHue::PhilipsHueClient']]],
  ['consolesample',['ConsoleSample',['../namespace_chroma_1_1_library_1_1_console_sample.html',1,'Chroma::Library']]],
  ['gaming',['Gaming',['../namespace_chroma_1_1_library_1_1_gaming.html',1,'Chroma.Library.Gaming'],['../namespace_chroma_1_1_library_1_1_philips_hue_1_1_gaming.html',1,'Chroma.Library.PhilipsHue.Gaming']]],
  ['interfaces',['Interfaces',['../namespace_chroma_1_1_library_1_1_interfaces.html',1,'Chroma::Library']]],
  ['library',['Library',['../namespace_chroma_1_1_library.html',1,'Chroma']]],
  ['castle_20core_20changelog',['Castle Core Changelog',['../md_src_packages__castle_8_core_84_82_81__c_h_a_n_g_e_l_o_g.html',1,'']]],
  ['philipshue',['PhilipsHue',['../namespace_chroma_1_1_library_1_1_philips_hue.html',1,'Chroma::Library']]],
  ['subsystems',['Subsystems',['../namespace_chroma_1_1_library_1_1_philips_hue_1_1_subsystems.html',1,'Chroma::Library::PhilipsHue']]],
  ['utils',['Utils',['../namespace_chroma_1_1_library_1_1_utils.html',1,'Chroma::Library']]],
  ['wrappedobjects',['WrappedObjects',['../namespace_chroma_1_1_library_1_1_wrapped_objects.html',1,'Chroma::Library']]]
];
