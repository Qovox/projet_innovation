var searchData=
[
  ['g',['G',['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a31ab43266f02cac2ef1a360ddabca7d7',1,'Chroma::Library::Color::RGBColor']]],
  ['gaminganimation',['GamingAnimation',['../class_chroma_1_1_library_1_1_gaming_1_1_gaming_animation.html',1,'Chroma::Library::Gaming']]],
  ['getbrightness',['GetBrightness',['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a4fd4d892eaae5121b5ec3980aba804df',1,'Chroma::Library::Color::RGBColor']]],
  ['gethue',['GetHue',['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#adfd659a45e9a869a636786fbd4380188',1,'Chroma::Library::Color::RGBColor']]],
  ['getlightbrightness',['GetLightBrightness',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#a98e22b331a4fa4bdec498c0624c601e4',1,'Chroma::Library::PhilipsHue::PhilipsHueClient']]],
  ['getlighthexcolor',['GetLightHexColor',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#a149634dbea6102b93ea49a48675fcd0a',1,'Chroma::Library::PhilipsHue::PhilipsHueClient']]],
  ['getlighthsvcolor',['GetLightHSVColor',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#a98ec124e8c7735cc0f8106dcf6756c99',1,'Chroma::Library::PhilipsHue::PhilipsHueClient']]],
  ['getlightrgbcolor',['GetLightRGBColor',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#a68b1f39128f14e6bd7b1df0bd783af09',1,'Chroma::Library::PhilipsHue::PhilipsHueClient']]],
  ['getlightstate',['GetLightState',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#ab0b67621eb7426625277dff94fe9d97a',1,'Chroma::Library::PhilipsHue::PhilipsHueClient']]],
  ['getsaturation',['GetSaturation',['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a9698e9e36b028b67ffecd01f13c0b623',1,'Chroma::Library::Color::RGBColor']]]
];
