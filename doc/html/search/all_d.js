var searchData=
[
  ['r',['R',['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#af33f87ecc94a733a6c4bd1f295d22f56',1,'Chroma::Library::Color::RGBColor']]],
  ['rgbcolor',['RGBColor',['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html',1,'Chroma.Library.Color.RGBColor'],['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a86ae4369b4a381185190ed9c3f9f1c6c',1,'Chroma.Library.Color.RGBColor.RGBColor(double red, double green, double blue)'],['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#afe3702ea40e47e8828c6cee98bee587f',1,'Chroma.Library.Color.RGBColor.RGBColor(int red, int green, int blue)'],['../class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#ad90f1f56d7444a041d811f39725099c7',1,'Chroma.Library.Color.RGBColor.RGBColor(string hexColor)']]]
];
