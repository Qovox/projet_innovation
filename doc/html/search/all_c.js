var searchData=
[
  ['philipshueanimation',['PhilipsHueAnimation',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html',1,'Chroma.Library.PhilipsHue.PhilipsHueAnimation'],['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html#ac1b8365a67b4fbed6d270ea7bbf9eb64',1,'Chroma.Library.PhilipsHue.PhilipsHueAnimation.PhilipsHueAnimation()']]],
  ['philipshueclient',['PhilipsHueClient',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html',1,'Chroma::Library::PhilipsHue']]],
  ['playanimation',['PlayAnimation',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#aae34844064364db25f0f8d2de245d9d2',1,'Chroma.Library.PhilipsHue.PhilipsHueClient.PlayAnimation(GamingAnimation animation, int numberOfLoops=1)'],['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html#afa6fb2cc12be08310b89d6f09c7c6ded',1,'Chroma.Library.PhilipsHue.PhilipsHueClient.PlayAnimation(PhilipsHueAnimation animation, int numberOfLoops=1)']]],
  ['preset',['Preset',['../namespace_chroma_1_1_library_1_1_utils.html#a65908f5c20b4d4342295089c5e330dda',1,'Chroma::Library::Utils']]],
  ['program',['Program',['../class_chroma_1_1_library_1_1_console_sample_1_1_program.html',1,'Chroma::Library::ConsoleSample']]]
];
