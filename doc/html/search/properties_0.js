var searchData=
[
  ['_5fbrightness',['_Brightness',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#a60a646457e193f9bac55c5284fe7c0bb',1,'Chroma::Library::PhilipsHue::PhilipsHueClient::LightChangeEventArgs']]],
  ['_5fhexcolor',['_HexColor',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#aae3d78924b76cc37ad38071bff602e5b',1,'Chroma::Library::PhilipsHue::PhilipsHueClient::LightChangeEventArgs']]],
  ['_5fhsvcolor',['_HSVColor',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#afa8a9e3d7a9f085e4675cf3bccc445ed',1,'Chroma::Library::PhilipsHue::PhilipsHueClient::LightChangeEventArgs']]],
  ['_5flightid',['_lightId',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#a917789156ddd4fbbcd0da948e77475a3',1,'Chroma::Library::PhilipsHue::PhilipsHueClient::LightChangeEventArgs']]],
  ['_5fon',['_On',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#ab88a65693b52a7cd0dbc7a73b33478c3',1,'Chroma::Library::PhilipsHue::PhilipsHueClient::LightChangeEventArgs']]],
  ['_5frgbcolor',['_RGBColor',['../class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html#a6acac3a3e661ac0e4fa2172db9457aaf',1,'Chroma::Library::PhilipsHue::PhilipsHueClient::LightChangeEventArgs']]]
];
