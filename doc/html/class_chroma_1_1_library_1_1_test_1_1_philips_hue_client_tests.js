var class_chroma_1_1_library_1_1_test_1_1_philips_hue_client_tests =
[
    [ "SetUp", "class_chroma_1_1_library_1_1_test_1_1_philips_hue_client_tests.html#a3014204aee1dd64c4bbb031152fddb79", null ],
    [ "TestCacheTimer", "class_chroma_1_1_library_1_1_test_1_1_philips_hue_client_tests.html#a3c528e2db0827207926e5397b0cbd713", null ],
    [ "TestCheckConnection", "class_chroma_1_1_library_1_1_test_1_1_philips_hue_client_tests.html#a80144eb65d4afba8b12e01c85d6972e9", null ],
    [ "TestNoBridgesFoundInitialize", "class_chroma_1_1_library_1_1_test_1_1_philips_hue_client_tests.html#af5f5604c60c1b5e7c2a42b3d6effe356", null ],
    [ "TestSettingsFile", "class_chroma_1_1_library_1_1_test_1_1_philips_hue_client_tests.html#ae5df769194119f3cdae9dae0a4593922", null ],
    [ "TestSuccessfulInitialize", "class_chroma_1_1_library_1_1_test_1_1_philips_hue_client_tests.html#a8f371f9f4db2d8af76c65241ad57d69c", null ]
];