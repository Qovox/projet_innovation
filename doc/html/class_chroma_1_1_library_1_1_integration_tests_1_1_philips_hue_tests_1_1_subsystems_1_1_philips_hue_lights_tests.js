var class_chroma_1_1_library_1_1_integration_tests_1_1_philips_hue_tests_1_1_subsystems_1_1_philips_hue_lights_tests =
[
    [ "SetUp", "class_chroma_1_1_library_1_1_integration_tests_1_1_philips_hue_tests_1_1_subsystems_1_1_philips_hue_lights_tests.html#a0354229e110dd1dcda35a994f7f921d2", null ],
    [ "TestGetLightBrightness", "class_chroma_1_1_library_1_1_integration_tests_1_1_philips_hue_tests_1_1_subsystems_1_1_philips_hue_lights_tests.html#af40bf8dde274003f2c91d708f032bc2d", null ],
    [ "TestGetLightRgbColor", "class_chroma_1_1_library_1_1_integration_tests_1_1_philips_hue_tests_1_1_subsystems_1_1_philips_hue_lights_tests.html#a32ff15845904c9c86447c7c4a6de0437", null ],
    [ "TestGetLightState", "class_chroma_1_1_library_1_1_integration_tests_1_1_philips_hue_tests_1_1_subsystems_1_1_philips_hue_lights_tests.html#a31bdd510081dd48b03cfae0c2c5e2785", null ],
    [ "TestSetLightColor", "class_chroma_1_1_library_1_1_integration_tests_1_1_philips_hue_tests_1_1_subsystems_1_1_philips_hue_lights_tests.html#a4849f3e92adfe67ff8b3c27bc24afc1e", null ],
    [ "TestSetLightsBrightness", "class_chroma_1_1_library_1_1_integration_tests_1_1_philips_hue_tests_1_1_subsystems_1_1_philips_hue_lights_tests.html#a8eb5f8f8388bb55d947ea31c0203cae1", null ]
];