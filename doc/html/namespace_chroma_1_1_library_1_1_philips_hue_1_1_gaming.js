var namespace_chroma_1_1_library_1_1_philips_hue_1_1_gaming =
[
    [ "EndGameAnimation", "class_chroma_1_1_library_1_1_philips_hue_1_1_gaming_1_1_end_game_animation.html", "class_chroma_1_1_library_1_1_philips_hue_1_1_gaming_1_1_end_game_animation" ],
    [ "ExplosionAnimation", "class_chroma_1_1_library_1_1_philips_hue_1_1_gaming_1_1_explosion_animation.html", "class_chroma_1_1_library_1_1_philips_hue_1_1_gaming_1_1_explosion_animation" ],
    [ "MorseCodeAnimation", "class_chroma_1_1_library_1_1_philips_hue_1_1_gaming_1_1_morse_code_animation.html", "class_chroma_1_1_library_1_1_philips_hue_1_1_gaming_1_1_morse_code_animation" ]
];