var interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation =
[
    [ "AppendAnimation", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#a417775067cf61cbefe8a7ab0a711b0de", null ],
    [ "Blink", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#a35ce1420ca0027fdebfd7194b0009ff6", null ],
    [ "ColorLoop", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#a34e3cd81f2eae223c1b4dd4cc1e8d65f", null ],
    [ "SetBrightness", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#a721e538aeb2141e885883f19ff03f519", null ],
    [ "SetColor", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#a2e2190a0bd2fe2febd0da1e7552e6dfb", null ],
    [ "SetColor", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#ad9dd152943db1937f18c2e4413fb15e4", null ],
    [ "TurnOff", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#a7d42f88c4580985678583f056ab97a81", null ],
    [ "TurnOn", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#ac2ddfee960e42a6109005e3e682f30f3", null ],
    [ "Wait", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html#aafc1f9236d6ebe27ac0d51316b3f682c", null ]
];