var class_chroma_1_1_library_1_1_color_1_1_r_g_b_color =
[
    [ "RGBColor", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a86ae4369b4a381185190ed9c3f9f1c6c", null ],
    [ "RGBColor", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#afe3702ea40e47e8828c6cee98bee587f", null ],
    [ "RGBColor", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#ad90f1f56d7444a041d811f39725099c7", null ],
    [ "GetBrightness", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a4fd4d892eaae5121b5ec3980aba804df", null ],
    [ "GetHue", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#adfd659a45e9a869a636786fbd4380188", null ],
    [ "GetSaturation", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a9698e9e36b028b67ffecd01f13c0b623", null ],
    [ "ToHex", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a7b2b7fc87bf90dd52ac7c59a6eef344b", null ],
    [ "ToHSV", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#ae522d755c78395ea8c6b32c8e30d99a9", null ],
    [ "B", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#ace48580de7486ed4c038890e5401ae43", null ],
    [ "G", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a31ab43266f02cac2ef1a360ddabca7d7", null ],
    [ "Q42RGBColor", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#a7da355616c81da870794427403b36212", null ],
    [ "R", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html#af33f87ecc94a733a6c4bd1f295d22f56", null ]
];