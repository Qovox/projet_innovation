var hierarchy =
[
    [ "EventArgs", null, [
      [ "Chroma.Library.PhilipsHue.PhilipsHueClient.LightChangeEventArgs", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client_1_1_light_change_event_args.html", null ]
    ] ],
    [ "Chroma.Library.Gaming.GamingAnimation", "class_chroma_1_1_library_1_1_gaming_1_1_gaming_animation.html", [
      [ "Chroma.Library.PhilipsHue.Gaming.EndGameAnimation", "class_chroma_1_1_library_1_1_philips_hue_1_1_gaming_1_1_end_game_animation.html", null ],
      [ "Chroma.Library.PhilipsHue.Gaming.ExplosionAnimation", "class_chroma_1_1_library_1_1_philips_hue_1_1_gaming_1_1_explosion_animation.html", null ],
      [ "Chroma.Library.PhilipsHue.Gaming.MorseCodeAnimation", "class_chroma_1_1_library_1_1_philips_hue_1_1_gaming_1_1_morse_code_animation.html", null ]
    ] ],
    [ "Chroma.Library.Color.HSVColor", "class_chroma_1_1_library_1_1_color_1_1_h_s_v_color.html", null ],
    [ "Chroma.Library.Interfaces.IHueAnimation", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_animation.html", [
      [ "Chroma.Library.PhilipsHue.PhilipsHueAnimation", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_animation.html", null ]
    ] ],
    [ "Chroma.Library.Interfaces.IHueClient", "interface_chroma_1_1_library_1_1_interfaces_1_1_i_hue_client.html", [
      [ "Chroma.Library.PhilipsHue.PhilipsHueClient", "class_chroma_1_1_library_1_1_philips_hue_1_1_philips_hue_client.html", null ]
    ] ],
    [ "LocatedBridge", null, [
      [ "Chroma.Library.WrappedObjects.LocatedBridge", "class_chroma_1_1_library_1_1_wrapped_objects_1_1_located_bridge.html", null ]
    ] ],
    [ "Chroma.Library.ConsoleSample.Program", "class_chroma_1_1_library_1_1_console_sample_1_1_program.html", null ],
    [ "Chroma.Library.Color.RGBColor", "class_chroma_1_1_library_1_1_color_1_1_r_g_b_color.html", null ]
];