# Chroma Library Changelog

## v3 (sprint3)

* Improve Animation Creator
* Gaming Animations for Philips Hue
  * End Game Animation
  * Explosion Animation
  * Morse Code Animation

## v2 (sprint2)

* Change brightness of a light
* Read the brightness of a light
* Animation Creator
* Performance increased
* Eventing

## v1 (sprint1)

* Connection to Philips Hue Bridge
* Turn On Lights
* Turn Off Lights
* Change color of lights
* Read the color of lights
