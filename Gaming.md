# Chroma.Library for Gaming

The Chroma Library can deliver usefull custom animations for gaming purpose.

## Morse Code Animation

Morse code Animation is a way of transmitting text information as a series of on-off lights.

```cs
// Play the Morse Code "SOS" in light 1
MorseCodeAnimation morse = new MorseCodeAnimation("sos", lights: 1);

// Play the animation in infinite loop (can be stopped)
client.PlayAnimation(morse, -1);
```

You can set custom paramaters to the constructor of a MorseCodeAnimation object :

* **color** : Color of the animation.
* **timeUnit** : Duration of a morse letter.
* **lights** : list of lights where the animation will be play.

## Explosion Animation

Let's your explosives blow in beautiful lights.

```cs
// Build Explosion Animation in lights 1 and 2 with colors Blue, Green and Cyan and add 3 subexplosions.
ExplosionAnimation exlosion = new ExplosionAnimation(
    lights: new int[] {1,2},
    colors: new RGBColor[] { ChromaColors.Blue, ChromaColors.Green, ChromaColors.Cyan },
    numberOfExplosions: 3);
client.PlayAnimation(explosion);
```

Explosion Animation contains also two animations preset :

* **Preset.One** :  Ligths color : White, **3 lights needed**
    1. Blink each lights.
* **Preset.Two** : Lights color : Yellow and Orange, **3 lights needed**
    1. Blink each lights in Yellow.
    2. Blink again each lights with low brightness in Orange.

You have to use the constructor of ExplosionAnimation to use one.

```cs
ExplosionAnimation explosionPreset1 = new ExplosionAnimation(Preset.One);
```

## EndGame Animation

Either if you win or lose, you'll find a way to display it with your lights.

```cs
// Build an EndGame Animation on lights 1 and 2 with a magenta color. The lights will blink 5 times during the animation
EndGameAnimation victoryAnimation = new EndGameAnimation(
    color: ChromaColors.Magenta,
    numberOfBlinks: 5,
    lights: new int[] {1,2});
client.PlayAnimation(victoryAnimation);
```

EndGame Animation contains also thee animations preset :

+ **Preset One**: Ligths color : Lime, **3 lights needed**
1. Blinks each lights alternatively 3 times in a row with the standard transition time.
2. Fades in to max brightness on each lights in 2500 ms.
3. Fades out to middle brightness on each lights in 1000 ms.

+ **Preset Two**: Ligths color : Lime, **3 lights needed**
1. Blinks each lights alternatively (with overlapping) with 300 ms as transition time.
2. Does the same things in step in reversed order.
3. Fades in to max brightness on each lights in 2500 ms.
4. Fades out to middle brightness on each lights in 1000 ms.

+ **Preset Three**: Lights color : Red, **3 lights needed**
Two lights are fading in until they reach their maximum brightness while the other one is blinking.

You have to use the constructor of EndGameAnimation to use one.
You can even precise a color to customize the preset.

```cs
EndGameAnimation endgameAnimationPresetOne = new EndGameAnimation(Preset.One, ChromaColors.Blue);
```