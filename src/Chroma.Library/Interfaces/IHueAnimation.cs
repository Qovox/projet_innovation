﻿using Chroma.Library.Color;
using System.Collections.Generic;

namespace Chroma.Library.Interfaces
{
    /// <summary>
    /// Interface to create animations.
    /// To implement whatever the platform.
    /// Stands on Fluent Interface design.
    /// </summary>
    public interface IHueAnimation
    {
        /// <summary>
        /// Appends an existing animation to this one.
        /// It can also be this current animation.
        /// </summary>
        /// <param name="animation">Either an existing animation or this animation itself.</param>
        /// <param name="numberOfAppend">The amount of time you wish to append this animation to the current one. If negative or zero, no append is made.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        IHueAnimation AppendAnimation(IHueAnimation animation, int numberOfAppend = 1);

        /// <summary>
        /// Turns on the lights.
        /// </summary>
        /// <param name="transitionTimeInMs">The transition time in miliseconds used to turn on the lights. If not provided, takes the default Philips Hue transition time value.</param>
        /// <param name="lights">The list of lights to turn on. If not provided, turns on all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        IHueAnimation TurnOn(int transitionTimeInMs = 400, params int[] lights);

        /// <summary>
        /// Turns off the lights.
        /// </summary>
        /// <param name="transitionTimeInMs">The transition time in miliseconds used to turn off the lights. If not provided, takes the default Philips Hue transition time value.</param>
        /// <param name="lights">The list of lights to turn off. If not provided, turns off all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        IHueAnimation TurnOff(int transitionTimeInMs = 400, params int[] lights);

        /// <summary>
        /// Sets the brightness of the lights.
        /// </summary>
        /// <param name="brightness">The brightness to set.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The list of lights to change the brightness. If not provided, sets the brightness on all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        IHueAnimation SetBrightness(float brightness = 1.0f, int transitionTimeInMs = 400, params int[] lights);

        /// <summary>
        /// Sets the color of the lights in RGB format.
        /// </summary>
        /// <param name="color">The RGB color to set.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The list of lights to change the color. If not provided, sets the color on all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        IHueAnimation SetColor(RGBColor color, int transitionTimeInMs = 400, params int[] lights);

        /// <summary>
        /// Sets the color of the lights in HSV format.
        /// </summary>
        /// <param name="color">The HSV color to set.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The list of lights to change the color. If not provided, sets the color on all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        IHueAnimation SetColor(HSVColor color, int transitionTimeInMs = 400, params int[] lights);

        /// <summary>
        /// Blinks the lights.
        /// </summary>
        /// <param name="numberOfBlinks">The number of blinks to perform. If it is either negative or zero, doesn't do anything.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The list of lights to blink. If not provided, blinks all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        IHueAnimation Blink(int numberOfBlinks = 1, int transitionTimeInMs = 400, params int[] lights);

        /// <summary>
        /// Loops over a set of colors.
        /// </summary>
        /// <param name="colors">The set of color through which iterates.</param>
        /// <param name="numberOfLoops">The number of loops.</param>
        /// <param name="transitionTimeInMs">The transition time between two colors. If not provided, takes the default platform value.</param>
        /// <param name="lights">The list of lights to loop colors. If not provided, loops on all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        IHueAnimation ColorLoop(IEnumerable<RGBColor> colors = null, int numberOfLoops = 1, int transitionTimeInMs = 400, params int[] lights);

        /// <summary>
        /// Wait a certain amount of time.
        /// </summary>
        /// <param name="waitTimeInMs">The time to wait. If negative or zero, doesn't do anything.</param>
        /// <param name="lights">The list of lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        IHueAnimation Wait(int waitTimeInMs, params int[] lights);
    }
}
