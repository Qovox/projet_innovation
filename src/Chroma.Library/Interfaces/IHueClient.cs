﻿using Chroma.Library.WrappedObjects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Chroma.Library.Interfaces
{
    /// <summary>
    /// Hue client interface to implement no matter the platform.
    /// </summary>
    public interface IHueClient
    {
        /// <summary>
        /// Scans network and search for bridges.
        /// </summary>
        /// <returns>A list of each scanned bridges.</returns>
        Task<List<LocatedBridge>> ScanBridgesAsync();

        /// <summary>
        /// Initializes connection with a bridge.
        /// </summary>
        /// <param name="bridgeIp">The Ip address of the bridge to connect to.</param>
        /// <param name="path">The target path to write settings.</param>
        /// <param name="cacheRefreshTime">The refresh rate for the cache.</param>
        /// <returns>True if the connection initialization succeeded, false otherwise.</returns>
        Task<bool> InitializeAsync(string bridgeIp = null, string path = "", int cacheRefreshTime = 100);

        /// <summary>
        /// Checks if the connection to the bridge established during initialization is effective.
        /// </summary>
        /// <returns>True if the connection to the bridge is effective, false otherwise.</returns>
        Task<bool> CheckConnectionAsync();
    }
}
