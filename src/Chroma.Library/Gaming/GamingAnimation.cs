﻿using Chroma.Library.Interfaces;
using Chroma.Library.Utils;
using System.Collections.Generic;

namespace Chroma.Library.Gaming
{
    /// <summary>
    /// A customizable gaming animation.
    /// </summary>
    public abstract class GamingAnimation
    {
        internal IHueAnimation Animation { get; set; }

        internal List<IHueAnimation> Animations { get; set; }

        internal bool IsParallel { get; set; } = false;

        internal Preset preset = Preset.None;

        internal abstract void BuildAnimation();
    }
}
