﻿namespace Chroma.Library.Utils
{
    /// <summary>
    /// Enum based on Philips Hue Api alert attribute.
    /// </summary>
    public enum BlinkMode
    {
        /// <summary>
        /// Stop blinking.
        /// </summary>
        None,

        /// <summary>
        /// Blink once.
        /// </summary>
        Once,

        /// <summary>
        /// Blink multiple times (during 15 seconds according to Philips Hue Api).
        /// </summary>
        Multiple
    }

    /// <summary>
    /// Specifies which setup mode applies to the animation.
    /// </summary>
    public enum SetupMode
    {
        /// <summary>
        /// Default setup mode. Does nothing.
        /// </summary>
        None,

        /// <summary>
        /// Turns on each given lights.
        /// </summary>
        TurnOnLights,

        /// <summary>
        /// Turns off each given lights.
        /// </summary>
        TurnOffLights
    }

    /// <summary>
    /// Animation preset mode.
    /// </summary>
    public enum Preset
    {
        /// <summary>
        /// No preset chosen.
        /// </summary>
        None,

        /// <summary>
        /// Preset one.
        /// Specify in the animation class what is the wanted effect.
        /// </summary>
        One,

        /// <summary>
        /// Preset two.
        /// Specify in the animation class what is the wanted effect.
        /// </summary>
        Two,

        /// <summary>
        /// Preset Three.
        /// Specify in the animation class what is the wanted effect.
        /// </summary>
        Three,
    }
}
