﻿using Chroma.Library.WrappedObjects;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Chroma.Library.UnitTests")]
[assembly: InternalsVisibleTo("Chroma.Library.IntegrationTests")]

namespace Chroma.Library.Utils
{
    /// <summary>
    /// Application data management class.
    /// </summary>
    internal static class ChromaAppData
    {
        private const string SettingsConfig = "settings.config";
        private static Settings Settings { get; set; }

        /// <summary>
        /// Either fecthes the data or initializes them.
        /// </summary>
        /// <param name="path">The target path to fetch data. Put a '/' at the end of the path.</param>
        public static void FetchSettings(string path = "")
        {
            if (File.Exists(path + SettingsConfig) && new FileInfo(path + SettingsConfig).Length != 0)
            {
                Settings = JsonConvert.DeserializeObject<Settings>(ReadFile(path + SettingsConfig));
            }
            else
            {
                Settings = new Settings();
            }
        }

        /// <summary>
        /// Gets bridge settings given a located bridge.
        /// </summary>
        /// <param name="bridge">The located bridge.</param>
        /// <returns>The bridge settings object corresponding to the located bridge if it exists, null otherwise.</returns>
        public static BridgeSettings GetBridgeSettings(LocatedBridge bridge)
        {
            foreach (BridgeSettings bs in Settings.bridges)
            {
                if (bs.properties != null && bs.properties.BridgeId.Equals(bridge.BridgeId))
                {
                    return bs;
                }
            }

            return null;
        }

        /// <summary>
        /// Adds new bridge settings to the application settings.
        /// </summary>
        /// <param name="bridgeSettings">The bridge settings to add.</param>
        /// <param name="path">The target path to write settings.</param>
        public static void AddBridgeSettings(BridgeSettings bridgeSettings, string path = "")
        {
            Settings.bridges.Add(bridgeSettings);
            SaveSettings(path);
        }

        /// <summary>
        /// Updates a new bridge settings to the application settings.
        /// </summary>
        /// <param name="bridgeSettings">The bridge settings to update.</param>
        /// <param name="appkey">The appkey from registration</param>
        /// <param name="path">The target path to write settings.</param>
        public static void UpdateBridgeSettings(BridgeSettings bridgeSettings, string appkey, string path = "")
        {
            //TODO Making GetBridgeSettings method returning a ref to a BridgeSettings would allow to modify the reference directly afterwards and thus reduce the lines of code to update the value into the hashshet.

            Settings.bridges.Remove(Settings.bridges.ToList()
                .Find(bs => bs.properties.BridgeId.Equals(bridgeSettings.properties.BridgeId)));
            bridgeSettings.appkey = appkey;
            Settings.bridges.Add(bridgeSettings);
            SaveSettings(path);
        }

        #region File Management

        /// <summary>
        /// Saves current settings.
        /// </summary>
        /// <param name="path">The target path to write settings.</param>
        public static void SaveSettings(string path = "")
        {
            if (!string.IsNullOrEmpty(path))
            {
                Directory.CreateDirectory(path);
            }

            WriteFile(path + SettingsConfig, JsonConvert.SerializeObject(Settings));
        }

        /// <summary>
        /// Creates a file given a path.
        /// </summary>
        /// <param name="path">The path of the file to create.</param>
        private static void CreateFile(string path)
        {
            File.Create(path).Dispose();
        }

        /// <summary>
        /// Reads a the content of a file given its path.
        /// </summary>
        /// <param name="path">The path of file to read.</param>
        /// <returns>The content of the file as a string value.</returns>
        private static string ReadFile(string path)
        {
            return File.ReadAllText(path);
        }

        /// <summary>
        /// Writes data into a file.
        /// </summary>
        /// <param name="path">The path of the file in which write.</param>
        /// <param name="data">The data to write in the file.</param>
        private static void WriteFile(string path, string data)
        {
            File.WriteAllText(path, data);
        }

        #endregion
    }

    #region Chroma App Data Objects

    internal class Settings
    {
        public HashSet<BridgeSettings> bridges { get; set; }

        public Settings()
        {
            bridges = new HashSet<BridgeSettings>();
        }
    }

    internal class BridgeSettings
    {
        public LocatedBridge properties { get; set; }

        public string appkey { get; set; }

        public BridgeSettings()
        {
            properties = new LocatedBridge();
        }
    }

    #endregion
}
