﻿using Chroma.Library.Color;
using Chroma.Library.Gaming;
using Chroma.Library.Interfaces;
using Chroma.Library.Utils;
using System.Collections.Generic;

namespace Chroma.Library.PhilipsHue.Gaming
{
    /// <summary>
    /// Customizable end game animation.
    /// </summary>
    public class EndGameAnimation : GamingAnimation
    {
        private RGBColor color;
        private int numberOfBlinks;
        private int blinkTransitionTimeInMs;
        private int[] lights;

        /// <summary>
        /// End game animation constructor allowing to customize the animation.
        /// </summary>
        /// <param name="color">The color to set to the lights for the animation.</param>
        /// <param name="numberOfBlinks">The number of blinks to perform.</param>
        /// <param name="blinkTransitionTimeInMs">The transition time determining how fast the blinks are.</param>
        /// <param name="lights">The lights to which apply the animation.</param>
        public EndGameAnimation(RGBColor color = null, int numberOfBlinks = 3, int blinkTransitionTimeInMs = 400, params int[] lights)
        {
            this.color = color ?? ChromaColors.Lime;
            this.numberOfBlinks = numberOfBlinks;
            this.blinkTransitionTimeInMs = blinkTransitionTimeInMs;
            this.lights = lights;

            BuildAnimation();
        }

        /// <summary>
        /// End game animation constructor using presets.
        /// 
        /// Preset One (Ligths color : Lime, 3 lights needed):
        /// 1. Blinks each lights alternatively 3 times in a row with the standard transition time.
        /// 2. Fades in to max brightness on each lights in 2500 ms.
        /// 3. Fades out to middle brightness on each lights in 1000 ms.
        /// 
        /// Preset Two (Ligths color : Lime, 3 lights needed):
        /// 1. Blinks each lights alternatively (with overlapping) with 300 ms as transition time.
        /// 2. Does the same things in step in reversed order.
        /// 3. Fades in to max brightness on each lights in 2500 ms.
        /// 4. Fades out to middle brightness on each lights in 1000 ms.
        /// 
        /// Preset Three (Lights color : Red, 3 lights needed):
        /// Two lights are fading in until they reach their maximum brightness while the other one is blinking.
        /// The blinking light will stop blinking as soon as the 2 others has reached the maximum brightness.
        /// </summary>
        /// <param name="preset">The preset to apply.</param>
        /// <param name="color">The color of the lights during the animation.</param>
        public EndGameAnimation(Preset preset, RGBColor color = null)
        {
            this.preset = preset;
            switch (preset)
            {
                case Preset.Three:
                    this.color = color ?? ChromaColors.Red;
                    break;
                default:
                    this.color = color ?? ChromaColors.Lime;
                    break;
            }

            BuildAnimation();
        }

        internal override void BuildAnimation()
        {
            PhilipsHueAnimation animationFrame;
            Animation = new PhilipsHueAnimation();

            switch (preset)
            {
                case Preset.One:
                    PhilipsHueAnimation blinkPattern = new PhilipsHueAnimation();
                    blinkPattern
                        .Blink(transitionTimeInMs: 100, lights: 1)
                        .Blink(transitionTimeInMs: 100, lights: 2)
                        .Blink(transitionTimeInMs: 100, lights: 3);
                    Animation
                        .SetColor(ChromaColors.Lime, 0)
                        .AppendAnimation(blinkPattern, 3)
                        .TurnOn(0)
                        .SetBrightness(transitionTimeInMs: 2500)
                        .SetBrightness(0.5f, 1000);
                    break;
                case Preset.Two:
                    IsParallel = true;
                    Animations = new List<IHueAnimation>();

                    animationFrame = new PhilipsHueAnimation();
                    animationFrame
                        .SetColor(color, 0)
                        .Blink(transitionTimeInMs: 300, lights: 1);
                    animationFrame.WaitAfterAnimation = 300;
                    Animations.Add(animationFrame);

                    animationFrame = new PhilipsHueAnimation();
                    animationFrame.Blink(transitionTimeInMs: 300, lights: 2);
                    animationFrame.WaitAfterAnimation = 300;
                    Animations.Add(animationFrame);

                    animationFrame = new PhilipsHueAnimation();
                    animationFrame
                        .Blink(transitionTimeInMs: 300, lights: 3)
                        .Wait(100)
                        .Blink(transitionTimeInMs: 300, lights: 3);
                    animationFrame.WaitAfterAnimation = 1100;
                    Animations.Add(animationFrame);

                    animationFrame = new PhilipsHueAnimation();
                    animationFrame.Blink(transitionTimeInMs: 300, lights: 2);
                    animationFrame.WaitAfterAnimation = 300;
                    Animations.Add(animationFrame);

                    animationFrame = new PhilipsHueAnimation();
                    animationFrame.Blink(transitionTimeInMs: 300, lights: 1);
                    animationFrame.WaitAfterAnimation = 1000;
                    Animations.Add(animationFrame);

                    animationFrame = new PhilipsHueAnimation();
                    animationFrame
                        .TurnOn(0)
                        .SetBrightness(transitionTimeInMs: 2500)
                        .SetBrightness(0.5f, 1200);
                    Animations.Add(animationFrame);
                    break;
                case Preset.Three:
                    IsParallel = true;
                    Animations = new List<IHueAnimation>();

                    animationFrame = new PhilipsHueAnimation();
                    animationFrame
                        .SetColor(color)
                        .TurnOn(0, lights: new int[] { 1, 3 })
                        .SetBrightness(brightness: 0, lights: new int[] { 1, 3 })
                        .Wait(400)
                        .SetBrightness(transitionTimeInMs: 2500, lights: new int[] { 1, 3 });
                    Animations.Add(animationFrame);

                    animationFrame = new PhilipsHueAnimation();
                    animationFrame
                        .Blink(12, 400, 2)
                        .TurnOn(0, 2)
                        .SetBrightness(lights: 2)
                        .SetBrightness(0.5f, 1000);
                    animationFrame.WaitAfterAnimation = 2700;
                    Animations.Add(animationFrame);
                    break;
                default:
                    Animation
                        .SetColor(color, 0, lights)
                        .Blink(numberOfBlinks, blinkTransitionTimeInMs, lights)
                        .TurnOn(0, lights)
                        .SetBrightness(transitionTimeInMs: blinkTransitionTimeInMs, lights: lights)
                        .SetBrightness(0.5f, 400 * numberOfBlinks, lights);
                    break;
            }
        }
    }
}
