﻿using Chroma.Library.Color;
using Chroma.Library.Gaming;
using Chroma.Library.Interfaces;
using System.Collections.Generic;

namespace Chroma.Library.PhilipsHue.Gaming
{
    /// <summary>
    /// Morse Code Animation builder.
    /// </summary>
    public class MorseCodeAnimation : GamingAnimation
    {
        #region Local Attributes

        private Dictionary<char, MorseLetter[]> MorseDictionary = new Dictionary<char, MorseLetter[]>();
        private string StringToCode;
        private int timeUnit;
        private int[] lights;
        private RGBColor color;

        #endregion

        /// <summary>
        /// Constructor for a customizable MorseCodeAnimation object.
        /// </summary>
        /// <param name="StringToCode">The word to transform into Morse code Animation.</param>
        /// <param name="timeUnit">The duration of a Morse Letter SHORT.</param>
        /// <param name="color"></param>
        /// <param name="lights">The IDs of the lights where the animation will be play.</param>
        public MorseCodeAnimation(string StringToCode, int timeUnit = 400, RGBColor color = null, params int[] lights)
        {
            InitiliazeDictionary();
            this.StringToCode = StringToCode;
            this.timeUnit = timeUnit;
            this.color = color ?? ChromaColors.White;
            this.lights = lights;
            BuildAnimation();
        }

        #region Internal and Private methods

        internal override void BuildAnimation()
        {
            Animation = new PhilipsHueAnimation();
            Animation.SetColor(color, lights: lights);

            foreach (char c in StringToCode)
            {
                foreach (MorseLetter ml in MorseDictionary[c])
                {
                    Animation.AppendAnimation(ml.GetAnimation(timeUnit, lights));
                    Animation.Wait(timeUnit / 2, lights);
                }
                Animation.Wait(3 * timeUnit, lights);
            }

            Animation.Wait(7 * timeUnit, lights);
        }

        private void InitiliazeDictionary()
        {
            // Fill letters.
            MorseDictionary.Add(' ', new MorseLetter[] { MorseLetter.SPACE });
            MorseDictionary.Add('a', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.LONG });
            MorseDictionary.Add('b', new MorseLetter[] { MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT });
            MorseDictionary.Add('c', new MorseLetter[] { MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.SHORT });
            MorseDictionary.Add('d', new MorseLetter[] { MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.SHORT });
            MorseDictionary.Add('e', new MorseLetter[] { MorseLetter.SHORT });
            MorseDictionary.Add('f', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.SHORT });
            MorseDictionary.Add('g', new MorseLetter[] { MorseLetter.LONG, MorseLetter.LONG, MorseLetter.SHORT });
            MorseDictionary.Add('h', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT });
            MorseDictionary.Add('i', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.SHORT });
            MorseDictionary.Add('j', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG });
            MorseDictionary.Add('k', new MorseLetter[] { MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.LONG });
            MorseDictionary.Add('l', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.SHORT });
            MorseDictionary.Add('m', new MorseLetter[] { MorseLetter.LONG, MorseLetter.LONG });
            MorseDictionary.Add('n', new MorseLetter[] { MorseLetter.LONG, MorseLetter.SHORT });
            MorseDictionary.Add('o', new MorseLetter[] { MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG });
            MorseDictionary.Add('p', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.LONG, MorseLetter.SHORT });
            MorseDictionary.Add('q', new MorseLetter[] { MorseLetter.LONG, MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.LONG });
            MorseDictionary.Add('r', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.SHORT });
            MorseDictionary.Add('s', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT });
            MorseDictionary.Add('t', new MorseLetter[] { MorseLetter.LONG });
            MorseDictionary.Add('u', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.LONG });
            MorseDictionary.Add('v', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.LONG });
            MorseDictionary.Add('w', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.LONG });
            MorseDictionary.Add('x', new MorseLetter[] { MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.LONG });
            MorseDictionary.Add('y', new MorseLetter[] { MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.LONG });
            MorseDictionary.Add('z', new MorseLetter[] { MorseLetter.LONG, MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.SHORT });

            // Fill numbers.
            MorseDictionary.Add('1', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG });
            MorseDictionary.Add('2', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG });
            MorseDictionary.Add('3', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.LONG, MorseLetter.LONG });
            MorseDictionary.Add('4', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.LONG });
            MorseDictionary.Add('5', new MorseLetter[] { MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT });
            MorseDictionary.Add('6', new MorseLetter[] { MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT });
            MorseDictionary.Add('7', new MorseLetter[] { MorseLetter.LONG, MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.SHORT, MorseLetter.SHORT });
            MorseDictionary.Add('8', new MorseLetter[] { MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG, MorseLetter.SHORT, MorseLetter.SHORT });
            MorseDictionary.Add('9', new MorseLetter[] { MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG, MorseLetter.SHORT });
            MorseDictionary.Add('0', new MorseLetter[] { MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG, MorseLetter.LONG });
        }

        #endregion
    }

    #region Morse Letter

    /// <summary>
    /// Different types of Morse Letters.
    /// </summary>
    enum MorseLetter
    {
        SHORT,
        LONG,
        SPACE
    }

    static class MorseLetterExtensionsMethods
    {
        public static IHueAnimation GetAnimation(this MorseLetter ml, int timeUnit, params int[] lights)
        {
            IHueAnimation Animation = new PhilipsHueAnimation();

            switch (ml)
            {
                case MorseLetter.SPACE:
                    Animation.Wait(5 * timeUnit, lights);
                    break;
                case MorseLetter.SHORT:
                    Animation.TurnOn(0, lights);
                    Animation.SetBrightness(0.5f, timeUnit / 2, lights);
                    Animation.TurnOff(timeUnit / 2, lights);
                    break;
                case MorseLetter.LONG:
                    Animation.TurnOn(0, lights);
                    Animation.SetBrightness(0.5f, timeUnit / 2, lights);
                    Animation.Wait(2 * timeUnit, lights);
                    Animation.TurnOff(timeUnit / 2, lights);
                    break;
                default:
                    break;
            }

            return Animation;
        }
    }

    #endregion
}
