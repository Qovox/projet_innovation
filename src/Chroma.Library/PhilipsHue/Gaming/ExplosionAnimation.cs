﻿using Chroma.Library.Color;
using Chroma.Library.Gaming;
using Chroma.Library.Interfaces;
using Chroma.Library.Utils;
using System;
using System.Collections.Generic;

namespace Chroma.Library.PhilipsHue.Gaming
{
    /// <summary>
    /// Customizable gaming explosion animation.
    /// </summary>
    public class ExplosionAnimation : GamingAnimation
    {
        #region Private Attributes

        private RGBColor[] colors;
        private int[] lights;
        private int numberOfExplosions;

        #endregion

        /// <summary>
        /// Explosion animation constructor allowing to customize the animation.
        /// </summary>
        /// <param name="colors">The colors to set to the lights for the animation.</param>
        /// <param name="numberOfExplosions">The number of explosions.</param>
        /// <param name="lights">The lights to which apply the animation.</param>
        public ExplosionAnimation(int[] lights, RGBColor[] colors = null, int numberOfExplosions = 1)
        {
            this.colors = colors ?? new RGBColor[] { ChromaColors.Yellow, ChromaColors.Red, ChromaColors.Orange };
            this.numberOfExplosions = numberOfExplosions;
            this.lights = lights;
            BuildAnimation();
        }

        /// <summary>
        /// Explosion animation constructor using presets.
        /// 
        /// Preset One (Ligths color : White, 3 lights needed):
        /// 1. Blinks each lights.
        /// 
        /// Preset Two (Lights color : Yellow and Orange, 3 lights needed):
        /// 1. Blinks each lights in Yellow.
        /// 2. Blinks again each lights with low brightness in Orange.
        /// </summary>
        /// <param name="preset">The preset to apply.</param>
        public ExplosionAnimation(Preset preset)
        {
            this.preset = preset;
            BuildAnimation();
        }

        #region Internal and Private Methods

        internal override void BuildAnimation()
        {
            Animation = new PhilipsHueAnimation();
            PhilipsHueAnimation animationFrame;
            switch (preset)
            {
                case Preset.One:
                    IsParallel = true;
                    Animations = new List<IHueAnimation>();

                    for (int i = 1; i < 4; i++)
                    {
                        animationFrame = new PhilipsHueAnimation();

                        animationFrame
                            .TurnOn(100, lights: i)
                            .SetColor(ChromaColors.White, 100, lights: i)
                            .TurnOff(100, lights: i);
                        Animations.Add(animationFrame);
                    }

                    break;
                case Preset.Two:
                    IsParallel = true;
                    Animations = new List<IHueAnimation>();

                    for (int i = 1; i < 4; i++)
                    {
                        animationFrame = new PhilipsHueAnimation();
                        animationFrame
                            .TurnOn(0, lights: i)
                            .SetColor(ChromaColors.Yellow, 0, lights: i)
                            .SetBrightness(1f, 100, lights: i)
                            .TurnOff(0, lights: i)
                            .Wait(100, lights: i)
                            .TurnOn(0, lights: i)
                            .SetColor(ChromaColors.Orange, 0, lights: i)
                            .SetBrightness(0.2f, 50, lights: i)
                            .TurnOff(0, lights: i);
                        Animations.Add(animationFrame);
                    }

                    break;
                default:
                    int nbColors = colors.Length;
                    int nbLights = lights.Length;
                    float brightness = 1f / nbColors;
                    Animation
                        .TurnOn(0, lights)
                        .SetBrightness(0f, 0, lights);

                    for (int i = 0; i < nbColors; i++)
                    {
                        Animation
                            .SetColor(colors[i], 0, lights)
                            .SetBrightness((i + 1) * brightness, 200, lights);
                    }
                    Animation.TurnOff();

                    if (numberOfExplosions > 1)
                    {
                        Random random = new Random();

                        for (int i = 0; i < numberOfExplosions; i++)
                        {
                            int randomId = random.Next(1, nbLights);
                            int randomColor = random.Next(0, nbColors);
                            Animation
                                .SetColor(colors[randomColor], 0, new int[] { lights[randomId] })
                                .TurnOn(0, new int[] { lights[randomId] })
                                .SetBrightness(0.5f, 200, new int[] { lights[randomId] })
                                .TurnOff(0, new int[] { lights[randomId] });
                        }
                    }
                    Animation
                        .Wait(400)
                        .TurnOff();
                    break;
            }
        }

        #endregion
    }
}
