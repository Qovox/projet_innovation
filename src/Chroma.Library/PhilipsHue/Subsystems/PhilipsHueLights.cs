﻿using Chroma.Library.Color;
using Chroma.Library.Utils;
using Q42.HueApi;
using Q42.HueApi.ColorConverters.HSB;
using Q42.HueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chroma.Library.PhilipsHue.Subsystems
{
    /// <summary>
    /// Philips Hue Lights subsystem.
    /// Manages lights and get / set lights attributes and states.
    /// </summary>
    internal class PhilipsHueLights
    {
        private ILocalHueClient client;
        private LightCommand lightCommand;

        public PhilipsHueLights(ILocalHueClient client)
        {
            this.client = client;
        }

        #region Lights Management

        /* Here we will find each methods related to the following sections in Philips Hue API:
         * Get all lights
         * Get new lights
         * Search for new lights
         * Delete ligth
         */

        #endregion

        #region Get / Set Lights Attributes And States

        #region Get / Set Lights Brightness

        /// <summary>
        /// Gets the brightness of a specific light.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>A float between 0 and 1 representing the brightness.</returns>
        public float GetLightBrightness(int lightId)
        {
            Light light = GetLightAsync(lightId);

            return light.State.Brightness / (float) 255.0;
        }

        /// <summary>
        /// Sets the brightness of either all lights or a specific one.
        /// </summary>
        /// <param name="brightness">The brightness between 0 and 1 to set to the light.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the brightness. If not provided, takes the default platform value.</param>
        /// <param name="lights">The ids of the lights. If not given sets the brightness for each lights.</param>
        public void SetLightsBrightness(float brightness, int transitionTimeInMs = 400, params int[] lights)
        {
            lightCommand = new LightCommand
            {
                // Philips Hue use Brightness between 0 and 255, so we need to convert.
                Brightness = Convert.ToByte(brightness * 255)
            };
            ApplyTransitionTime(ref lightCommand, transitionTimeInMs);

            SendLightCommand(lightCommand, lights);
        }

        #endregion

        #region Get / Set Lights Color

        /// <summary>
        /// Gets the color of a specific light and returns it under RGB color format.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>The current RGB color of the light.</returns>
        public RGBColor GetLightRGBColor(int lightId)
        {
            Light light = GetLightAsync(lightId);

            return new RGBColor(StateExtensions.ToRgb(light.State));
        }

        /// <summary>
        /// Gets the color of a specific light and returns it under HSV color format.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>The current HSV color of the light.</returns>
        public HSVColor GetLightHSVColor(int lightId)
        {
            Light light = GetLightAsync(lightId);

            int hue = light.State.Hue ?? 0;
            int saturation = light.State.Saturation ?? 0;

            return new HSVColor(hue, saturation, light.State.Brightness);
        }

        /// <summary>
        /// Gets the color of a specific light and returns it under hexadecimal color format.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>The current hexadecimal color of the light.</returns>
        public string GetLightHexColor(int lightId)
        {
            Light light = GetLightAsync(lightId);

            return LightExtensions.ToHex(light);
        }

        /// <summary>
        /// Sets the color of either all lights or a specific one.
        /// </summary>
        /// <param name="color">The color to set to the light in RGB format.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The ids of the light. If not given sets the color for each lights.</param>
        public void SetLightsColor(RGBColor color, int transitionTimeInMs = 400, params int[] lights)
        {
            lightCommand = new LightCommand().SetColor(color.Q42RGBColor);
            ApplyTransitionTime(ref lightCommand, transitionTimeInMs);

            SendLightCommand(lightCommand, lights);
        }

        /// <summary>
        /// Sets the color of either all lights or a specific one.
        /// </summary>
        /// <param name="color">The color to set to the light in HSV format.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The ids of the light. If not given sets the color for each lights.</param>
        public void SetLightsColor(HSVColor color, int transitionTimeInMs = 400, params int[] lights)
        {
            lightCommand = new LightCommand().SetColor(color.ToRGB().Q42RGBColor);
            ApplyTransitionTime(ref lightCommand, transitionTimeInMs);

            SendLightCommand(lightCommand, lights);
        }

        #endregion

        #region Get / Set Light States

        /// <summary>
        /// Turns on either all the lights or a specific one.
        /// </summary>
        /// <param name="lightCommand">The light command waiting to be executed.</param>
        /// <param name="transitionTimeInMs">The transition time used to turn on the lights. If not provided, takes the default platform value.</param>
        /// <param name="lights">The ids of the lights. If not provided turns on each lights.</param>
        public void TurnOnLights(LightCommand lightCommand = null, int transitionTimeInMs = 400, params int[] lights)
        {
            if (lightCommand == null)
            {
                lightCommand = new LightCommand
                {
                    On = true
                };
            }
            else
            {
                lightCommand.On = true;
            }

            ApplyTransitionTime(ref lightCommand, transitionTimeInMs);

            SendLightCommand(lightCommand, lights);
        }

        /// <summary>
        /// Turns off either all the lights or a specific one.
        /// </summary>
        /// <param name="lightCommand">The light command waiting to be executed.</param>
        /// <param name="transitionTimeInMs">The transition time used to turn off the lights. If not provided, takes the default platform value.</param>
        /// <param name="lights">The ids of the lights. If not provided turns off each lights.</param>
        public void TurnOffLights(LightCommand lightCommand = null, int transitionTimeInMs = 400, params int[] lights)
        {
            if (lightCommand == null)
            {
                lightCommand = new LightCommand
                {
                    On = false
                };
            }
            else
            {
                lightCommand.On = false;
            }

            lightCommand.TransitionTime = TimeSpan.FromMilliseconds(transitionTimeInMs);

            SendLightCommand(lightCommand, lights);
        }

        /// <summary>
        /// Gets the state of a specific light.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>True if the light is On, False otherwise.</returns>
        public bool GetLightState(int lightId)
        {
            Light light = GetLightAsync(lightId);

            return light.State.On;
        }

        #endregion

        #region Light Effects

        /// <summary>
        /// Blinks either all the lights or a specific one given a blink mode.
        /// This method uses predefined alert mode provided by Philips Hue.
        /// </summary>
        /// <param name="blinkMode">The blink mode.</param>
        /// <param name="lights">The ids of the lights. If not provided blinks each lights.</param>
        public void BlinkLights(BlinkMode blinkMode, params int[] lights)
        {
            switch (blinkMode)
            {
                case BlinkMode.Once:
                    lightCommand = new LightCommand()
                    {
                        Alert = Alert.Once
                    };
                    break;
                case BlinkMode.Multiple:
                    lightCommand = new LightCommand()
                    {
                        Alert = Alert.Multiple
                    };
                    break;
                default:
                    return;
            }

            SendLightCommand(lightCommand, lights);
        }

        /// <summary>
        /// Stops blinking the lights.
        /// </summary>
        /// <param name="lights">The ids of the lights to make stop blinking.</param>
        public void StopBlinkingLights(params int[] lights)
        {
            lightCommand = new LightCommand()
            {
                Alert = Alert.None
            };

            SendLightCommand(lightCommand, lights);
        }

        /// <summary>
        /// Applies a color loop effect on lights.
        /// This method uses predefined effect mode provided by Philips Hue.
        /// </summary>
        /// <param name="lights">The ids of the lights. If not provided loops on each lights.</param>
        public void ColorLoopOnLights(params int[] lights)
        {
            lightCommand = new LightCommand()
            {
                Effect = Effect.ColorLoop
            };

            SendLightCommand(lightCommand, lights);
        }

        /// <summary>
        /// Stops color looping on the lights.
        /// </summary>
        /// <param name="lights">The ids of the lights to make stop color looping.</param>
        public void StopColorLoopingOnLights(params int[] lights)
        {
            lightCommand = new LightCommand()
            {
                Effect = Effect.None
            };

            SendLightCommand(lightCommand, lights);
        }

        #endregion

        #endregion

        #region Useful Methods

        public void ApplyTransitionTime(ref LightCommand lightCommand, int transitionTimeInMs)
        {
            if (transitionTimeInMs != 400)
            {
                lightCommand.TransitionTime = TimeSpan.FromMilliseconds(transitionTimeInMs);
            }
        }

        /// <summary>
        /// Gets a light status given its light id.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>A light object containing the light status.</returns>
        public Light GetLightAsync(int lightId)
        {
            return client.GetLightAsync(lightId.ToString()).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Sends a light command to one or more lights.
        /// </summary>
        /// <param name="lightCommand">The light command to send to the lights.</param>
        /// <param name="lights">The lights to send the command to.</param>
        private async void SendLightCommand(LightCommand lightCommand, params int[] lights)
        {
            if (lights == null || !lights.Any())
            {
                await client.SendCommandAsync(lightCommand).ConfigureAwait(false);
            }
            else
            {
                List<string> stringLights = new List<string>();

                foreach (int light in lights)
                {
                    stringLights.Add(light.ToString());
                }

                await client.SendCommandAsync(lightCommand, stringLights).ConfigureAwait(false);
            }
        }

        #endregion
    }
}
