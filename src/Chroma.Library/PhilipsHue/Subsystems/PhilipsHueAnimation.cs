﻿using Chroma.Library.Color;
using Chroma.Library.Interfaces;
using Chroma.Library.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Chroma.Library.UnitTests")]
[assembly: InternalsVisibleTo("Chroma.Library.IntegrationTests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace Chroma.Library.PhilipsHue
{
    /// <summary>
    /// Philips Hue animation creator tool.
    /// Allows to create animation with chaining method style.
    /// Be careful when applying the transition time.
    /// For instance, setting the lights color with a 1000ms transition time whilst the lights are not emitting any light, and then the turning on the lights with a transition time of 400ms will result in turning on the light with the previous color set with a 400ms transition time.
    /// Hence you should always set the transition time you wish to apply on the latest effective light state.
    /// </summary>
    public class PhilipsHueAnimation : IHueAnimation
    {
        /// <summary>
        /// Indicates whether the animation is playing or not.
        /// </summary>
        public bool IsStopped { get; internal set; } = true;

        internal bool IsModified { get; set; } = false;

        internal int WaitAfterAnimation { get; set; } = 0;

        internal List<AnimationFrame> Frames { get; set; } = new List<AnimationFrame>();

        internal List<int> Lights { get; set; } = new List<int>();

        internal List<int> ModifiedLights { get; set; } = new List<int>();

        /// <summary>
        /// Constructor of an animation providing.
        /// </summary>
        /// <param name="client">The client allowing to perform the setup.</param>
        /// <param name="setupMode">The setup mode.</param>
        /// <param name="color">The rgb color to set to the lights provided./></param>
        /// <param name="setupTransitionTimeInMs">The transition time in miliseconds to apply the setup.</param>
        /// <param name="lights">The lights the setup will affect. If no lights are provided, the setup and the color will apply on each available lights.</param>
        public PhilipsHueAnimation(PhilipsHueClient client = null, SetupMode setupMode = SetupMode.None, RGBColor color = null, int setupTransitionTimeInMs = 400, params int[] lights)
        {
            if (client != null)
            {
                if (color != null)
                {
                    client.SetLightsColor(color, lights: lights);
                }

                switch (setupMode)
                {
                    case SetupMode.None:
                        break;
                    case SetupMode.TurnOnLights:
                        client.TurnOnLights(setupTransitionTimeInMs, lights);
                        break;
                    case SetupMode.TurnOffLights:
                        client.TurnOffLights(setupTransitionTimeInMs, lights);
                        break;
                }
            }
        }

        internal void Reset()
        {
            IsStopped = true;
            IsModified = false;
            ModifiedLights = Lights.ToList();

            foreach (AnimationFrame frame in Frames)
            {
                frame.Reset();
            }
        }

        internal void UpdateLights(params int[] lights)
        {
            if (lights.Any())
            {
                Lights.AddRange(lights.Except(Lights));
                ModifiedLights.AddRange(Lights.Except(ModifiedLights));
            }
            else
            {
                // TODO Handle the case where no light is passed as parameter.
            }
        }

        internal void RemoveLight(int lightId)
        {
            IsModified = true;
            ModifiedLights.Remove(lightId);

            if (ModifiedLights.Any())
            {
                foreach (AnimationFrame frame in Frames)
                {
                    frame.IsModified = IsModified;
                    frame.ModifiedLights.Remove(lightId);
                }
            }
            else
            {
                Reset();
            }
        }

        /// <summary>
        /// Appends an existing animation to this one.
        /// It can also be this current animation.
        /// </summary>
        /// <param name="animation">Either an existing animation or this animation itself.</param>
        /// <param name="numberOfAppend">The amount of time you wish to append this animation to the current one. If negative or zero, no append is made.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        public IHueAnimation AppendAnimation(IHueAnimation animation, int numberOfAppend = 1)
        {
            if (animation.GetType().IsAssignableFrom(GetType()))
            {
                PhilipsHueAnimation castedAnimation = animation as PhilipsHueAnimation;

                for (int i = 0; i < numberOfAppend; ++i)
                {
                    Frames.AddRange(castedAnimation.Frames);
                }
            }

            return this;
        }

        /// <summary>
        /// Turns on the lights.
        /// </summary>
        /// <param name="transitionTimeInMs">The transition time in miliseconds used to turn on the lights. If not provided, takes the default Philips Hue transition time value.</param>
        /// <param name="lights">The list of lights to turn on. If not provided, turns on all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        public IHueAnimation TurnOn(int transitionTimeInMs = 400, params int[] lights)
        {
            UpdateLights(lights);
            Frames.Add(new TurnOnFrame(transitionTimeInMs, lights));
            return this;
        }

        /// <summary>
        /// Turns off the lights.
        /// </summary>
        /// <param name="transitionTimeInMs">The transition time in miliseconds used to turn off the lights. If not provided, takes the default Philips Hue transition time value.</param>
        /// <param name="lights">The list of lights to turn off. If not provided, turns off all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        public IHueAnimation TurnOff(int transitionTimeInMs = 400, params int[] lights)
        {
            UpdateLights(lights);
            Frames.Add(new TurnOffFrame(transitionTimeInMs, lights));
            return this;
        }

        /// <summary>
        /// Sets the brightness of the lights.
        /// </summary>
        /// <param name="brightness">The brightness to set.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The list of lights to change the brightness. If not provided, sets the brightness on all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        public IHueAnimation SetBrightness(float brightness = 1.0f, int transitionTimeInMs = 400, params int[] lights)
        {
            UpdateLights(lights);
            Frames.Add(new SetBrightnessColorFrame(brightness, transitionTimeInMs, lights));
            return this;
        }

        /// <summary>
        /// Sets the color of the lights in RGB format.
        /// </summary>
        /// <param name="color">The RGB color to set.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The list of lights to change the color. If not provided, sets the color on all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        public IHueAnimation SetColor(RGBColor color, int transitionTimeInMs = 400, params int[] lights)
        {
            UpdateLights(lights);
            Frames.Add(new SetRGBColorFrame(color, transitionTimeInMs, lights));
            return this;
        }

        /// <summary>
        /// Sets the color of the lights in HSV format.
        /// </summary>
        /// <param name="color">The HSV color to set.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The list of lights to change the color. If not provided, sets the color on all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        public IHueAnimation SetColor(HSVColor color, int transitionTimeInMs = 400, params int[] lights)
        {
            UpdateLights(lights);
            Frames.Add(new SetHSVColorFrame(color, transitionTimeInMs, lights));
            return this;
        }

        /// <summary>
        /// Blinks the lights.
        /// </summary>
        /// <param name="numberOfBlinks">The number of blinks to perform. If it is either negative or zero, doesn't do anything.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The list of lights to blink. If not provided, blinks all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        public IHueAnimation Blink(int numberOfBlinks = 1, int transitionTimeInMs = 400, params int[] lights)
        {
            UpdateLights(lights);
            Frames.Add(new TurnOffFrame(transitionTimeInMs, lights));

            for (int i = 0; i < numberOfBlinks; ++i)
            {
                Frames.Add(new TurnOnFrame(0, lights));
                SetBrightness(transitionTimeInMs: transitionTimeInMs, lights: lights);
                Frames.Add(new TurnOffFrame(transitionTimeInMs, lights));
            }

            return this;
        }

        /// <summary>
        /// Loops over a set of colors.
        /// </summary>
        /// <param name="colors">The set of color through which iterates.</param>
        /// <param name="numberOfLoops">The number of loops.</param>
        /// <param name="transitionTimeInMs">The transition time between two colors. If not provided, takes the default platform value.</param>
        /// <param name="lights">The list of lights to loop colors. If not provided, loops on all the lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        public IHueAnimation ColorLoop(IEnumerable<RGBColor> colors = null, int numberOfLoops = 1, int transitionTimeInMs = 400, params int[] lights)
        {
            UpdateLights(lights);

            List<AnimationFrame> frames = new List<AnimationFrame>();

            foreach (RGBColor color in colors)
            {
                frames.Add(new SetRGBColorFrame(color, transitionTimeInMs, lights));
            }

            for (int i = 0; i < numberOfLoops; ++i)
            {
                Frames.AddRange(frames);
            }

            return this;
        }

        /// <summary>
        /// Wait a certain amount of time.
        /// </summary>
        /// <param name="waitTime">The time to wait. If negative or zero, doesn't do anything.</param>
        /// <param name="lights">The list of lights.</param>
        /// <returns>An IHueAnimation object to chain the animation creation.</returns>
        public IHueAnimation Wait(int waitTime = 0, params int[] lights)
        {
            UpdateLights(lights);

            if (waitTime != 0)
            {
                Frames.Add(new WaitFrame(waitTime, lights));
            }

            return this;
        }
    }

    #region Animation Frames

    internal abstract class AnimationFrame
    {
        public int TransitionTimeInMs { get; protected set; }

        public bool IsModified { get; set; } = false;

        public IEnumerable<int> Lights { get; set; }

        public List<int> ModifiedLights { get; set; }

        public AnimationFrame(int transitionTimeInMs, params int[] lights)
        {
            TransitionTimeInMs = transitionTimeInMs;
            Lights = lights;
            ModifiedLights = lights.ToList();
        }

        public abstract void Execute(PhilipsHueClient client);

        public void Reset()
        {
            IsModified = false;
            ModifiedLights = Lights.ToList();
        }
    }

    internal class TurnOnFrame : AnimationFrame
    {
        public TurnOnFrame(int transitionTimeInMs, params int[] lights)
            : base(transitionTimeInMs, lights)
        {

        }

        public override void Execute(PhilipsHueClient client)
        {
            if (IsModified)
            {
                client.TurnOnLights(TransitionTimeInMs, Lights.ToArray());
            }
            else
            {
                client.TurnOnLights(TransitionTimeInMs, ModifiedLights.ToArray());
            }
        }
    }

    internal class TurnOffFrame : AnimationFrame
    {
        public TurnOffFrame(int transitionTimeInMs, params int[] lights)
            : base(transitionTimeInMs, lights)
        {

        }

        public override void Execute(PhilipsHueClient client)
        {
            if (IsModified)
            {
                client.TurnOffLights(TransitionTimeInMs, Lights.ToArray());
            }
            else
            {
                client.TurnOffLights(TransitionTimeInMs, ModifiedLights.ToArray());
            }
        }
    }

    internal class SetRGBColorFrame : AnimationFrame
    {
        private RGBColor color;

        public SetRGBColorFrame(RGBColor color, int transitionTimeInMs, params int[] lights)
            : base(transitionTimeInMs, lights)
        {
            this.color = color;
        }

        public override void Execute(PhilipsHueClient client)
        {
            if (IsModified)
            {
                client.SetLightsColor(color, TransitionTimeInMs, Lights.ToArray());
            }
            else
            {
                client.SetLightsColor(color, TransitionTimeInMs, ModifiedLights.ToArray());
            }
        }
    }

    internal class SetHSVColorFrame : AnimationFrame
    {
        private HSVColor color;

        public SetHSVColorFrame(HSVColor color, int transitionTimeInMs, params int[] lights)
            : base(transitionTimeInMs, lights)
        {
            this.color = color;
        }

        public override void Execute(PhilipsHueClient client)
        {
            if (IsModified)
            {
                client.SetLightsColor(color, TransitionTimeInMs, Lights.ToArray());
            }
            else
            {
                client.SetLightsColor(color, TransitionTimeInMs, ModifiedLights.ToArray());
            }
        }
    }

    internal class SetBrightnessColorFrame : AnimationFrame
    {
        private float brightness;

        public SetBrightnessColorFrame(float brightness, int transitionTimeInMs, params int[] lights)
            : base(transitionTimeInMs, lights)
        {
            this.brightness = brightness;
        }

        public override void Execute(PhilipsHueClient client)
        {
            if (IsModified)
            {
                client.SetLightsBrightness(brightness, TransitionTimeInMs, Lights.ToArray());
            }
            else
            {
                client.SetLightsBrightness(brightness, TransitionTimeInMs, ModifiedLights.ToArray());
            }
        }
    }

    internal class WaitFrame : AnimationFrame
    {
        public WaitFrame(int waitTime, params int[] lights)
            : base(waitTime, lights)
        {

        }

        public override void Execute(PhilipsHueClient client)
        {
            // Do nothing
        }
    }

    #endregion
}
