﻿using Chroma.Library.PhilipsHue.Subsystems;
using Q42.HueApi.Interfaces;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Chroma.Library.UnitTests")]

namespace Chroma.Library.PhilipsHue
{
    /// <summary>
    /// Philips Hue client.
    /// It provides every available methods to send requests to the Philips Hue bridge.
    /// </summary>
    public partial class PhilipsHueClient : Interfaces.IHueClient
    {
        private ILocalHueClient client;

        /// <summary>
        /// Initializes every subsystems.
        /// </summary>
        private void InitializeSubsystems()
        {
            lightsSubsystem = new PhilipsHueLights(client);
        }

        /// <summary>
        /// Details from the last exception.
        /// </summary>
        public static string LastExceptionDetails { get; internal set; }
    }
}
