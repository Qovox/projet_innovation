﻿using Chroma.Library.Utils;
using Chroma.Library.WrappedObjects;
using Q42.HueApi;
using Q42.HueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Chroma.Library.PhilipsHue
{
    public partial class PhilipsHueClient
    {
        /// <summary>
        /// Bridge linked to this client.
        /// </summary>
        public LocatedBridge LinkedBridge { get; private set; }

        /// <summary>
        /// Username used for this hue client bridge.
        /// </summary>
        public string Username { get; private set; }

        /// <inheritdoc/>
        /// <summary>
        /// Scans network and search for Philips Hue bridges.
        /// </summary>
        /// <returns>A list of each Philips Hue scanned bridges.</returns>
        public async virtual Task<List<LocatedBridge>> ScanBridgesAsync()
        {
            IBridgeLocator locator = new HttpBridgeLocator();
            var locatedBridges = await locator.LocateBridgesAsync(TimeSpan.FromSeconds(5)).ConfigureAwait(false);

            var enumerator = locatedBridges.GetEnumerator();
            List<LocatedBridge> scannedBridges = new List<LocatedBridge>();

            while (enumerator.MoveNext())
            {
                scannedBridges.Add(new LocatedBridge(enumerator.Current));
            }

            enumerator.Dispose();
            return scannedBridges;
        }

        /// <summary>
        /// Initializes connection with a bridge.
        /// </summary>
        /// <param name="bridgeIp">The Ip address of the bridge to connect to.</param>
        /// <param name="path">The target path to write settings.</param>
        /// <param name="cacheRefreshTime">The refresh rate for the cache.</param>
        /// <returns>True if the connection initialization succeeded, false otherwise.</returns>
        public async virtual Task<bool> InitializeAsync(string bridgeIp = null, string path = "", int cacheRefreshTime = 100)
        {
            ChromaAppData.FetchSettings(path);

            if (string.IsNullOrEmpty(bridgeIp))
            {
                List<LocatedBridge> scannedBridges = await ScanBridgesAsync().ConfigureAwait(false);

                if (scannedBridges.Any())
                {
                    bridgeIp = scannedBridges[0].IpAddress;
                    LinkedBridge = scannedBridges[0];
                }
                else
                {
                    LastExceptionDetails = "No Philips Hue bridges detected.";
                    return false;
                }
            }

            client = new LocalHueClient(bridgeIp);
            InitializeSubsystems();
            BridgeSettings bridgeSettings = ChromaAppData.GetBridgeSettings(LinkedBridge);

            if (bridgeSettings == null)
            {
                Username = await GetUsername().ConfigureAwait(false);

                bridgeSettings = new BridgeSettings
                {
                    properties = LinkedBridge,
                    appkey = Username
                };

                ChromaAppData.AddBridgeSettings(bridgeSettings, path);
                return !string.IsNullOrEmpty(Username) && await CheckConnectionAsync().ConfigureAwait(false);
            }

            if (string.IsNullOrEmpty(bridgeSettings.appkey))
            {
                Username = await GetUsername().ConfigureAwait(false);

                ChromaAppData.UpdateBridgeSettings(bridgeSettings, Username, path);
                return !string.IsNullOrEmpty(Username) && await CheckConnectionAsync().ConfigureAwait(false);
            }

            Username = bridgeSettings.appkey;
            client.Initialize(bridgeSettings.appkey);
            InitializeCache(cacheRefreshTime);

            return await CheckConnectionAsync().ConfigureAwait(false);
        }

        /// <summary>
        /// Requests a registration and gets the resulting username.
        /// </summary>
        /// <returns>The string representing the username to send requests to the Philips Hue API.</returns>
        private async Task<string> GetUsername()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            while (sw.ElapsedMilliseconds < 60000)
            {
                try
                {
                    return await client.RegisterAsync("chroma", "chroma_app").ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    if (!e.Message.Equals("Link button not pressed"))
                    {
                        LastExceptionDetails = e.Message;
                        return null;
                    }
                }
            }

            LastExceptionDetails = "Time out exception!";
            return null;
        }

        /// <summary>
        /// Checks if the connection to the bridge established during initialization is effective.
        /// </summary>
        /// <returns>True if the connection is effective, false otherwise.</returns>
        public async virtual Task<bool> CheckConnectionAsync()
        {
            return await client.CheckConnection().ConfigureAwait(false);
        }
    }
}
