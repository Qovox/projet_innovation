﻿using Chroma.Library.Color;
using System;

namespace Chroma.Library.PhilipsHue
{
    public partial class PhilipsHueClient
    {
        /// <summary>
        /// Event raised when a light has changed.
        /// </summary>
        public event LightChangeEventHandler LightChangeEvent;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public delegate void LightChangeEventHandler(LightChangeEventArgs e);

        /// <summary>
        /// Method called when LightChangeEvent is raised.
        /// </summary>
        protected virtual void OnLightChangeEventRaised(LightChangeEventArgs e)
        {
            LightChangeEvent?.Invoke(e);
        }

        #region Events Data

        /// <summary>
        /// Data send when LightChangeEventHandler is raised.
        /// </summary>
        public class LightChangeEventArgs : EventArgs
        {
            /// <summary>
            /// The Id of the light.
            /// </summary>
            public int _lightId { get; internal set; }

            /// <summary>
            /// True if light is On, false otherwise.
            /// </summary>
            public bool _On { get; internal set; }

            /// <summary>
            /// The light color in RGB.
            /// </summary>
            public RGBColor _RGBColor { get; internal set; }

            /// <summary>
            /// The light color in HSV.
            /// </summary>
            public HSVColor _HSVColor { get; internal set; }

            /// <summary>
            /// The light color in HEX.
            /// </summary>
            public string _HexColor { get; internal set; }

            /// <summary>
            /// The light brightness.
            /// </summary>
            public float _Brightness { get; internal set; }

            /// <summary>
            /// Default LightChangeEventArgs constructor.
            /// </summary>
            public LightChangeEventArgs()
            {

            }
        }

        #endregion
    }
}
