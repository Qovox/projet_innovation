﻿using Chroma.Library.Color;
using Chroma.Library.PhilipsHue.Subsystems;
using Chroma.Library.Utils;
using Q42.HueApi;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chroma.Library.PhilipsHue
{
    public partial class PhilipsHueClient
    {
        private PhilipsHueLights lightsSubsystem;

        #region Get Color

        /// <summary>
        /// Gets the color of a specific light and returns it under RGB color format.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>The current RGB color of the light.</returns>
        public RGBColor GetLightRGBColor(int lightId)
        {
            if (LightIdExists(lightId))
            {
                RGBColor color = lightsSubsystem.GetLightRGBColor(lightId);
                UpdateLightInCacheColor(color.ToHSV(), lightId);

                return color;
            }

            LastExceptionDetails = "Unknown light ID.";
            return null;
        }

        /// <summary>
        /// Gets the color of a specific light and returns it under RGB color format.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>The current HSV color of the light.</returns>
        public HSVColor GetLightHSVColor(int lightId)
        {

            if (LightIdExists(lightId))
            {
                HSVColor color = lightsSubsystem.GetLightHSVColor(lightId);
                UpdateLightInCacheColor(color, lightId);

                return color;
            }

            LastExceptionDetails = "Unknown light ID.";
            return null;
        }

        /// <summary>
        /// Gets the color of a specific light and returns it under hexadecimal color format.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>The current hexadecimal color of the light.</returns>
        public string GetLightHexColor(int lightId)
        {
            if (LightIdExists(lightId))
            {
                string color = lightsSubsystem.GetLightHexColor(lightId);
                UpdateLightInCacheColor(new RGBColor(color).ToHSV(), lightId);

                return color;
            }

            LastExceptionDetails = "Unknown light ID.";
            return null;
        }

        #endregion

        /// <summary>
        /// Sets the color of either all lights or a specific one.
        /// </summary>
        /// <param name="color">The color to set to the light in RGB format.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The ids of the lights. If not given sets the color for each lights.</param>
        public void SetLightsColor(RGBColor color, int transitionTimeInMs = 400, params int[] lights)
        {
            List<int> cleanList = new List<int>();
            CheckLights(ref cleanList, lights);

            for (int i = 0; i < cleanList.Count; ++i)
            {
                Light lightStatus = GetLightInCache(cleanList[i]);

                if (!lightStatus.State.On)
                {
                    UpdateLightInCacheColor(color.ToHSV(), cleanList[i]);
                    UpdateLightInCacheStatus(true, cleanList[i]);

                    LightCommand lightCommand = new LightCommand()
                    {
                        Hue = Convert.ToInt32(color.GetHue()),
                        Saturation = Convert.ToInt16(color.GetSaturation()),
                        Brightness = Convert.ToByte(color.GetBrightness())
                    };

                    AddLightInCacheCommand(lightCommand, cleanList[i]);
                    cleanList.Remove(cleanList[i--]);
                }
            }

            if (cleanList.Any())
            {
                lightsSubsystem.SetLightsColor(color, transitionTimeInMs, cleanList.ToArray());
            }
        }

        /// <summary>
        /// Sets the color of either all lights or a specific one.
        /// </summary>
        /// <param name="color">The color to set to the light in HSV format.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the color. If not provided, takes the default platform value.</param>
        /// <param name="lights">The ids of the lights. If not given sets the color for each lights.</param>
        public void SetLightsColor(HSVColor color, int transitionTimeInMs = 400, params int[] lights)
        {
            List<int> cleanList = new List<int>();
            CheckLights(ref cleanList, lights);

            for (int i = 0; i < cleanList.Count; ++i)
            {
                Light lightStatus = GetLightInCache(cleanList[i]);

                if (!lightStatus.State.On)
                {
                    UpdateLightInCacheColor(color, cleanList[i]);
                    UpdateLightInCacheStatus(true, cleanList[i]);

                    LightCommand lightCommand = new LightCommand()
                    {
                        Hue = color.Hue,
                        Saturation = color.Saturation,
                        Brightness = Convert.ToByte(color.Value)
                    };

                    AddLightInCacheCommand(lightCommand, cleanList[i]);
                    cleanList.Remove(cleanList[i--]);
                }
            }

            if (cleanList.Any())
            {
                lightsSubsystem.SetLightsColor(color, transitionTimeInMs, cleanList.ToArray());
            }
        }

        /// <summary>
        /// Gets the brightness of a specific light and returns it as float between 0 and 1.
        /// </summary>
        /// <param name="lightId">The ID of the light.</param>
        /// <returns></returns>
        public float GetLightBrightness(int lightId)
        {
            if (LightIdExists(lightId))
            {
                float brightness = lightsSubsystem.GetLightBrightness(lightId);
                UpdateLightInCacheBrightness(brightness, lightId);

                return brightness;
            }

            LastExceptionDetails = "Unknown light ID.";
            return new float();
        }

        /// <summary>
        /// Sets the brightness of either all lights or a specific one.
        /// </summary>
        /// <param name="brightness">The brightness between 0 and 1 to set to the light.</param>
        /// <param name="transitionTimeInMs">The transition time used to set the brightness. If not provided, takes the default platform value.</param>
        /// <param name="lights">The ids of the lights. If not given sets the brightness for each lights.</param>
        public void SetLightsBrightness(float brightness, int transitionTimeInMs = 400, params int[] lights)
        {
            List<int> cleanList = new List<int>();
            CheckLights(ref cleanList, lights);

            for (int i = 0; i < cleanList.Count; ++i)
            {
                Light lightStatus = GetLightInCache(cleanList[i]);

                if (!lightStatus.State.On && lightStatus.State.Brightness != Convert.ToByte(brightness * 255))
                {
                    UpdateLightInCacheBrightness(brightness, cleanList[i]);
                    UpdateLightInCacheStatus(true, cleanList[i]);

                    LightCommand lightCommand = new LightCommand()
                    {
                        Brightness = Convert.ToByte(brightness * 255)
                    };

                    AddLightInCacheCommand(lightCommand, cleanList[i]);
                    cleanList.Remove(cleanList[i--]);
                }
            }

            if (cleanList.Any())
            {
                lightsSubsystem.SetLightsBrightness(brightness, transitionTimeInMs, cleanList.ToArray());
            }
        }

        /// <summary>
        /// Turns on either all the lights connected to the bridge or a specific one.
        /// </summary>
        /// <param name="transitionTimeInMs">The transition time used to turn on the lights. If not provided, takes the default platform value.</param>
        /// <param name="lights">The ids of the lights. If not provided turns on all the lights.</param>
        public void TurnOnLights(int transitionTimeInMs = 400, params int[] lights)
        {
            List<int> cleanList = new List<int>();
            CheckLights(ref cleanList, lights);

            for (int i = 0; i < cleanList.Count; ++i)
            {
                Light lightStatus = GetLightInCache(cleanList[i]);

                if (!lightStatus.State.On)
                {
                    lightStatus.State.On = true;
                    UpdateLightInCache(lightStatus, cleanList[i]);

                    if (IsLightStatusWaiting(cleanList[i]))
                    {
                        lightsSubsystem.TurnOnLights(GetLightInCacheCommand(cleanList[i]), transitionTimeInMs, cleanList[i]);
                        RemoveLightInCacheCommand(cleanList[i]);
                        UpdateLightInCacheStatus(false, cleanList[i]);
                        cleanList.Remove(cleanList[i--]);
                    }
                }
                else
                {
                    cleanList.Remove(cleanList[i--]);
                }
            }

            if (cleanList.Any())
            {
                lightsSubsystem.TurnOnLights(transitionTimeInMs: transitionTimeInMs, lights: cleanList.ToArray());
            }
        }

        /// <summary>
        /// Turns off either all the lights connected to the bridge or a specific one.
        /// </summary>
        /// <param name="transitionTimeInMs">The transition time used to turn off the lights. If not provided, takes the default platform value.</param>
        /// <param name="lights">The ids of the lights. If not provided turns off all the lights.</param>
        public void TurnOffLights(int transitionTimeInMs = 400, params int[] lights)
        {
            List<int> cleanList = new List<int>();
            CheckLights(ref cleanList, lights);

            for (int i = 0; i < cleanList.Count; ++i)
            {
                Light lightStatus = GetLightInCache(cleanList[i]);

                if (lightStatus.State.On)
                {
                    lightStatus.State.On = false;
                    UpdateLightInCache(lightStatus, cleanList[i]);

                    if (IsLightStatusWaiting(cleanList[i]))
                    {
                        lightsSubsystem.TurnOffLights(GetLightInCacheCommand(cleanList[i]), transitionTimeInMs, cleanList[i]);
                        RemoveLightInCacheCommand(cleanList[i]);
                        UpdateLightInCacheStatus(false, cleanList[i]);
                        cleanList.Remove(cleanList[i--]);
                    }
                }
                else
                {
                    cleanList.Remove(cleanList[i--]);
                }
            }

            if (cleanList.Any())
            {
                lightsSubsystem.TurnOffLights(transitionTimeInMs: transitionTimeInMs, lights: cleanList.ToArray());
            }
        }

        /// <summary>
        /// Gets the state of a specific light.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>True if the light is On, False otherwise.</returns>
        public bool GetLightState(int lightId)
        {
            if (LightIdExists(lightId))
            {
                bool state = lightsSubsystem.GetLightState(lightId);
                UpdateLightInCacheState(state, lightId);

                return state;
            }

            LastExceptionDetails = "Unknown light ID.";
            return false;
        }

        /// <summary>
        /// Blinks either all the lights or a specific one given a blink mode.
        /// This method uses predefined alert mode provided by Philips Hue.
        /// </summary>
        /// <param name="blinkMode">The blink mode.</param>
        /// <param name="lights">The ids of the lights. If not provided blinks </param>
        public void BlinkLights(BlinkMode blinkMode, params int[] lights)
        {
            lightsSubsystem.BlinkLights(blinkMode, lights);
        }

        /// <summary>
        /// Stops blinking the lights.
        /// </summary>
        /// <param name="lights">The ids of the lights to make stop blinking.</param>
        public void StopBlinkingLights(params int[] lights)
        {
            lightsSubsystem.StopBlinkingLights(lights);
        }

        /// <summary>
        /// Applies a color loop effect on lights.
        /// This method uses predefined effect mode provided by Philips Hue.
        /// </summary>
        /// <param name="lights">The ids of the lights. If not provided loops on each lights.</param>
        public void ColorLoopOnLights(params int[] lights)
        {
            lightsSubsystem.ColorLoopOnLights(lights);
        }

        /// <summary>
        /// Stops color looping on the lights.
        /// </summary>
        /// <param name="lights">The ids of the lights to make stop color looping.</param>
        public void StopColorLoopingOnLights(params int[] lights)
        {
            lightsSubsystem.StopColorLoopingOnLights(lights);
        }
    }
}
