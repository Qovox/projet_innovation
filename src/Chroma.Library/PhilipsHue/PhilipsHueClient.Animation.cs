﻿using Chroma.Library.Gaming;
using Chroma.Library.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Chroma.Library.PhilipsHue
{
    public partial class PhilipsHueClient
    {
        /// <summary>
        /// Plays a gaming animation.
        /// </summary>
        /// <param name="animation">The gaming animation to be played.</param>
        /// <param name="numberOfLoops">The number of time to play the animation. If not provided, loops one time. To loop infinitely, provide a negative value.</param>
        public void PlayAnimation(GamingAnimation animation, int numberOfLoops = 1)
        {
            if (animation.IsParallel)
            {
                PlayParallelAnimation(animation.Animations);
            }
            else
            {
                PlayAnimation(animation.Animation as PhilipsHueAnimation, numberOfLoops);
            }
        }

        /// <summary>
        /// Plays a classic animation.
        /// </summary>
        /// <param name="animation">The animation to be played.</param>
        /// <param name="numberOfLoops">The number of time to play the animation. If not provided, loops once. To loop infinitely, provide a negative value.</param>
        public async void PlayAnimation(PhilipsHueAnimation animation, int numberOfLoops = 1)
        {
            int modifier = 1;

            if (numberOfLoops != 0)
            {
                if (numberOfLoops < 0)
                {
                    numberOfLoops = 1;
                    modifier = 0;
                }

                StopAnimation(animation.Lights.ToArray());

                foreach (int light in animation.Lights.ToArray())
                {
                    AddLightInCacheAnimated(animation, light);
                }
            }

            animation.IsStopped = false;

            for (int i = 0; i < numberOfLoops; i += modifier)
            {
                foreach (var frame in animation.Frames)
                {
                    if (!animation.IsStopped)
                    {
                        await Task.Run(() =>
                        {
                            frame.Execute(this);
                            Thread.Sleep(frame.TransitionTimeInMs);
                        });
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }

        private async void PlayParallelAnimation(List<IHueAnimation> animations)
        {
            foreach (PhilipsHueAnimation animation in animations)
            {
                await Task.Run(() =>
                {
                    PlayAnimation(animation);
                    Thread.Sleep(animation.WaitAfterAnimation);
                });
            }
        }

        /// <summary>
        /// Stops the animation if there's one currently playing, does nothing otherwise.
        /// </summary>
        /// <param name="lights">The light on which stop animation.</param>
        public void StopAnimation(params int[] lights)
        {
            foreach (int light in lights)
            {
                PhilipsHueAnimation animation = GetLightInCacheAnimated(light);

                if (animation != null)
                {
                    animation.RemoveLight(light);
                    RemoveLightsInCacheAnimated(light);
                }
            }
        }
    }
}
