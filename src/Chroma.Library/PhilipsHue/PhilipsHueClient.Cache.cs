﻿using Chroma.Library.Color;
using Q42.HueApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Timers;

[assembly: InternalsVisibleTo("Chroma.Library.UnitTests")]

namespace Chroma.Library.PhilipsHue
{
    public partial class PhilipsHueClient
    {
        #region Members

        private const string Status = "_status";
        private const string Command = "_command";
        private const string Animated = "_animated";

        internal ObjectCache cache = MemoryCache.Default;

        private CacheItemPolicy DefaultCacheItemPolicy { get; } = new CacheItemPolicy
        {
            AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration,
            SlidingExpiration = ObjectCache.NoSlidingExpiration
        };

        internal Timer cacheTimer;

        internal int TotalLights { get; private set; } = 0;

        #endregion

        /// <summary>
        /// Initializes the cache and his timer. Sets the expirations properties.
        /// </summary>
        private async void InitializeCache(int cacheRefreshTime)
        {
            IEnumerable<Light> enumLights = await GetLightsAsync();

            foreach (Light light in enumLights)
            {
                cache.Add(light.Id, light, DefaultCacheItemPolicy);
                cache.Add(light.Id + Status, false, DefaultCacheItemPolicy);
                TotalLights++;
            }

            cacheTimer = new Timer
            {
                Interval = cacheRefreshTime,
                AutoReset = true
            };
            cacheTimer.Elapsed += OnTimedEventUpdateCache;
        }

        #region Auto-Update

        /// <summary>
        /// Starts auto-updating the cache.
        /// </summary>
        public void StartCacheAutoUpdate()
        {
            cacheTimer.Start();
        }

        /// <summary>
        /// Stops auto-updating the cache.
        /// </summary>
        public void StopCacheAutoUpdate()
        {
            cacheTimer.Stop();
        }

        /// <summary>
        /// Method elapsed by the cacheTimer to update the cache. 
        /// </summary>
        /// <param name="source">Source of the Elapsed Event.</param>
        /// <param name="e">the Elapsed Event.</param>
        private void OnTimedEventUpdateCache(Object source, ElapsedEventArgs e)
        {
            IEnumerable<Light> enumLights = GetLightsAsync().Result;

            foreach (Light light in enumLights)
            {
                Light oldLight = (Light) cache.Get(light.Id);

                if (HasChanged(oldLight, light))
                {
                    LightChangeEventArgs args = new LightChangeEventArgs();
                    args._lightId = Int16.Parse(light.Id);
                    args._On = light.State.On;
                    args._HSVColor = new HSVColor(light.State.Hue ?? 0, light.State.Saturation ?? 0, light.State.Brightness);
                    args._RGBColor = args._HSVColor.ToRGB();
                    args._HexColor = args._HSVColor.ToHex();
                    args._Brightness = light.State.Brightness;
                    OnLightChangeEventRaised(args);
                }

                cache.Set(light.Id, light, DefaultCacheItemPolicy);
            }
        }

        /// <summary>
        /// Says if a light has changed between his old and newer value.
        /// </summary>
        /// <param name="oldLight">The old value of the light.</param>
        /// <param name="light">The newer value of the light.</param>
        /// <returns></returns>
        private bool HasChanged(Light oldLight, Light light)
        {
            return ((oldLight.State.On != light.State.On) || (oldLight.State.Hue != light.State.Hue) ||
                    (oldLight.State.Brightness != light.State.Brightness));
        }

        #endregion

        #region Specific Add Update Get

        private void AddLightInCacheCommand(LightCommand command, int lightId)
        {
            cache.Set(lightId + Command, command, DefaultCacheItemPolicy);
        }

        private void AddLightInCacheAnimated(PhilipsHueAnimation animation, int lightId)
        {
            cache.Set(lightId + Animated, animation, DefaultCacheItemPolicy);
        }

        private void RemoveLightInCacheCommand(int lightId)
        {
            cache.Remove(lightId + Command);
        }

        private void RemoveLightsInCacheAnimated(int lightId)
        {
            cache.Remove(lightId + Animated);
        }

        private LightCommand GetLightInCacheCommand(int lightId)
        {
            return cache.Get(lightId + Command) as LightCommand;
        }

        private PhilipsHueAnimation GetLightInCacheAnimated(int lightId)
        {
            return cache.Get(lightId + Animated) as PhilipsHueAnimation;
        }

        private void UpdateLightInCacheColor(HSVColor color, int lightId)
        {
            Light lightInCache = GetLightInCache(lightId);
            lightInCache.State.Hue = color.Hue;
            lightInCache.State.Saturation = color.Saturation;
            lightInCache.State.Brightness = Convert.ToByte(color.Value);

            UpdateLightInCache(lightInCache, lightId);
        }

        private void UpdateLightInCacheBrightness(float brightness, int lightId)
        {
            Light lightInCache = GetLightInCache(lightId);
            lightInCache.State.Brightness = Convert.ToByte(brightness * 255);

            UpdateLightInCache(lightInCache, lightId);
        }

        private void UpdateLightInCacheStatus(bool waiting, int lightId)
        {
            cache.Set(lightId + Status, waiting, DefaultCacheItemPolicy);
        }

        private void UpdateLightInCacheState(bool state, int lightId)
        {
            Light lightInCache = GetLightInCache(lightId);
            lightInCache.State.On = state;

            UpdateLightInCache(lightInCache, lightId);
        }

        #endregion

        #region Check, Get, Set

        /// <summary>
        /// Checks if a given light id exists.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>True if the light exists in the cache, false otherwise.</returns>
        private bool LightIdExists(int lightId)
        {
            return cache.Contains(lightId.ToString());
        }

        private bool IsLightStatusWaiting(int lightId)
        {
            return Convert.ToBoolean(cache.Get(lightId + Status));
        }

        private void CheckLights(ref List<int> cleanList, IEnumerable<int> lights)
        {
            if (lights.Any())
            {
                cleanList = DistinctLights(lights).ToList();
            }
            else
            {
                for (int i = 1; i <= TotalLights; ++i)
                    cleanList.Add(i);
            }
        }

        private IEnumerable<int> DistinctLights(IEnumerable<int> lights)
        {
            if (lights.Count() > 1)
            {
                lights = lights.Distinct().ToList();
            }

            foreach (int light in lights)
            {
                if (LightIdExists(light))
                {
                    yield return light;
                }
            }
        }

        /// <summary>
        /// Gets the light corresponding to the light id given.
        /// </summary>
        /// <param name="lightId">The id of the light.</param>
        /// <returns>The light object corresponding to the light id given.</returns>
        private Light GetLightInCache(int lightId)
        {
            return cache.Get(lightId.ToString()) as Light;
        }

        private void UpdateLightInCache(Light light, int lightId)
        {
            cache.Set(lightId.ToString(), light, DefaultCacheItemPolicy);
        }

        #endregion

        /// <summary>
        /// Gets lights status.
        /// </summary>
        /// <returns>An Enumerable of lights objects containing the lights status.</returns>
        private async Task<IEnumerable<Light>> GetLightsAsync()
        {
            return await client.GetLightsAsync();
        }
    }
}
