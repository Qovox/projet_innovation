﻿using System;
using System.Globalization;
using System.Linq;

namespace Chroma.Library.Color
{
    /// <summary>
    /// Chroma RHB color inspired from Q42.HueApi.ColorConverters.RGBColor.
    /// </summary>
    public class RGBColor
    {
        /// <summary>
        /// Red color.
        /// </summary>
        public double R { get; set; }

        /// <summary>
        /// Green color.
        /// </summary>
        public double G { get; set; }

        /// <summary>
        /// Blue color.
        /// </summary>
        public double B { get; set; }

        internal Q42.HueApi.ColorConverters.RGBColor Q42RGBColor { get; set; }

        /// <summary>
        /// Internal constructor allowing to get Chroma RGBColor object from a Q42 RGBColor.
        /// </summary>
        /// <param name="Q42RGBColor">The RGBColor object from Q42 API to get colors from.</param>
        internal RGBColor(Q42.HueApi.ColorConverters.RGBColor Q42RGBColor)
        {
            R = Q42RGBColor.R;
            G = Q42RGBColor.G;
            B = Q42RGBColor.B;

            this.Q42RGBColor = Q42RGBColor;
        }

        /// <summary>
        /// RGB Color constructor taking double values.
        /// </summary>
        /// <param name="red">Red color between 0.0 and 1.0</param>
        /// <param name="green">Green color between 0.0 and 1.0</param>
        /// <param name="blue">Blue color between 0.0 and 1.0</param>
        public RGBColor(double red, double green, double blue)
        {
            if (red < 0)
                red = 0;
            else if (red > 1)
                red = 1;

            if (green < 0)
                green = 0;
            else if (green > 1)
                green = 1;

            if (blue < 0)
                blue = 0;
            else if (blue > 1)
                blue = 1;

            R = red;
            G = green;
            B = blue;

            Q42RGBColor = new Q42.HueApi.ColorConverters.RGBColor(R, G, B);
        }

        /// <summary>
        /// RGB Color constructor taking int values.
        /// </summary>
        /// <param name="red">Red color between 0 and 255.</param>
        /// <param name="green">Green color between 0 and 255.</param>
        /// <param name="blue">Blue color between 0 and 255.</param>
        public RGBColor(int red, int green, int blue)
        {
            red = red > 255 ? 255 : red;
            green = green > 255 ? 255 : green;
            blue = blue > 255 ? 255 : blue;

            red = red < 0 ? 0 : red;
            green = green < 0 ? 0 : green;
            blue = blue < 0 ? 0 : blue;

            R = red / 255.0;
            G = green / 255.0;
            B = blue / 255.0;

            Q42RGBColor = new Q42.HueApi.ColorConverters.RGBColor(R, G, B);
        }

        /// <summary>
        /// RGB Color from hexadecimal format.
        /// </summary>
        /// <param name="hexColor">The color under hexadecimal format.</param>
        public RGBColor(string hexColor)
        {
            if (string.IsNullOrEmpty(hexColor))
                throw new ArgumentNullException(nameof(hexColor));

            // Clean hexColor value, remove the #.
            hexColor = hexColor.Replace("#", string.Empty).Trim();

            if (hexColor.Length != 6)
                throw new ArgumentException("hexColor should contains 6 characters", nameof(hexColor));

            int red = int.Parse(hexColor.Substring(0, 2), NumberStyles.AllowHexSpecifier);
            int green = int.Parse(hexColor.Substring(2, 2), NumberStyles.AllowHexSpecifier);
            int blue = int.Parse(hexColor.Substring(4, 2), NumberStyles.AllowHexSpecifier);

            R = red / 255.0;
            G = green / 255.0;
            B = blue / 255.0;

            Q42RGBColor = new Q42.HueApi.ColorConverters.RGBColor(R, G, B);
        }

        /// <summary>
        /// Method based on Q42 internal method from Q42.HueApi.ColorConverters.HSB.RGBExtensions.
        /// </summary>
        /// <returns>An HSV color format representing this color.</returns>
        public HSVColor ToHSV()
        {
            return new HSVColor((int) GetHue(), (int) GetSaturation(), (int) GetBrightness());
        }

        /// <summary>
        /// Method based on Q42 internal method from Q42.HueApi.ColorConverters.HSB.RGBExtensions.
        /// </summary>
        /// <returns>The equivalent hue of this color.</returns>
        public double GetHue()
        {
            if (R == G && G == B)
            {
                return 0;
            }

            double hue;

            var min = Min(R, G, B);
            var max = Max(R, G, B);

            var delta = max - min;

            if (AlmostEquals(R, max))
            {
                hue = (G - B) / delta; // Between yellow & magenta.
            }
            else if (AlmostEquals(G, max))
            {
                hue = 2 + (B - R) / delta; // Between cyan & yellow.
            }
            else
            {
                hue = 4 + (R - G) / delta; // Between magenta & cyan.
            }

            hue *= 60; // Degrees.

            if (hue < 0)
            {
                hue += 360;
            }

            return hue * 182.04f;
        }

        /// <summary>
        /// Method based on Q42 internal method from Q42.HueApi.ColorConverters.HSB.RGBExtensions.
        /// </summary>
        /// <returns>The equivalent saturation of this color.</returns>
        public double GetSaturation()
        {
            var min = Min(R, G, B);
            var max = Max(R, G, B);

            if (AlmostEquals(max, min))
            {
                return 0;
            }

            return ((AlmostEquals(max, 0f)) ? 0f : 1f - (1f * min / max)) * 255;
        }

        /// <summary>
        /// Method based on Q42 internal method from Q42.HueApi.ColorConverters.HSB.RGBExtensions.
        /// </summary>
        /// <returns>The equivalent brightness of this color.</returns>
        public double GetBrightness()
        {
            return Max(R, G, B) * 255;
        }

        /// <summary>
		/// Tests equality with a certain amount of precision. Default to smallest possible double.
		/// </summary>
		/// <param name="a">First value</param>
		/// <param name="b">Second value</param> 
		/// <param name="precision">Optional, smallest possible double value.</param>
		/// <returns>True if a and b are almost equals, false otherwise.</returns>
		private bool AlmostEquals(double a, double b, double precision = float.Epsilon)
        {
            return Math.Abs(a - b) <= precision;
        }

        private double Max(params double[] numbers)
        {
            return numbers.Max();
        }

        private double Min(params double[] numbers)
        {
            return numbers.Min();
        }

        /// <summary>
        /// Converts this color as a six-digit hexadecimal string, in the form RRGGBB.
        /// </summary>
        /// <returns>A six-digit hexadecimal string representing this color.</returns>
        public string ToHex()
        {
            int red = (int) (R * 255.99);
            int green = (int) (G * 255.99);
            int blue = (int) (B * 255.99);

            return string.Format("{0}{1}{2}", red.ToString("X2"), green.ToString("X2"), blue.ToString("X2"));
        }
    }
}
