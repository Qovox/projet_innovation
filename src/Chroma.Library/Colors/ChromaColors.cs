﻿namespace Chroma.Library.Color
{
    /// <summary>
    /// Contains classic primary colors.
    /// </summary>
    public static class ChromaColors
    {
        /// <summary>
        /// White RGB color (255 255 255).
        /// </summary>
        public static readonly RGBColor White = new RGBColor(255, 255, 255);

        /// <summary>
        /// Red RGB Color (255 0 0).
        /// </summary>
        public static readonly RGBColor Red = new RGBColor(255, 0, 0);

        /// <summary>
        /// Lime RGB Color (0 255 0).
        /// </summary>
        public static readonly RGBColor Lime = new RGBColor(0, 255, 0);

        /// <summary>
        /// Blue RGB Color (0 0 255).
        /// </summary>
        public static readonly RGBColor Blue = new RGBColor(0, 0, 255);

        /// <summary>
        /// Yellow RGB Color (255 0 0).
        /// </summary>
        public static readonly RGBColor Yellow = new RGBColor(255, 255, 0);

        /// <summary>
        /// Cyan RGB Color (0 255 255).
        /// </summary>
        public static readonly RGBColor Cyan = new RGBColor(0, 255, 255);

        /// <summary>
        /// Magenta RGB Color (255 0 255).
        /// </summary>
        public static readonly RGBColor Magenta = new RGBColor(255, 0, 255);

        /// <summary>
        /// Silver RGB Color (192 192 192).
        /// </summary>
        public static readonly RGBColor Silver = new RGBColor(192, 192, 192);

        /// <summary>
        /// Gray RGB Color (128 128 128).
        /// </summary>
        public static readonly RGBColor Gray = new RGBColor(128, 128, 128);

        /// <summary>
        /// Maroon RGB Color (128 0 0).
        /// </summary>
        public static readonly RGBColor Maroon = new RGBColor(128, 0, 0);

        /// <summary>
        /// Olive RGB Color (128 128 0).
        /// </summary>
        public static readonly RGBColor Olive = new RGBColor(128, 128, 0);

        /// <summary>
        /// Green RGB Color (0 128 0).
        /// </summary>
        public static readonly RGBColor Green = new RGBColor(0, 128, 0);

        /// <summary>
        /// Purple RGB Color (128 0 128).
        /// </summary>
        public static readonly RGBColor Purple = new RGBColor(128, 0, 128);

        /// <summary>
        /// Teal RGB Color (0 128 128).
        /// </summary>
        public static readonly RGBColor Teal = new RGBColor(0, 128, 128);

        /// <summary>
        /// Navy RGB Color (0 0 128).
        /// </summary>
        public static readonly RGBColor Navy = new RGBColor(0, 0, 128);

        /// <summary>
        /// Orange RGB Color (255 128 0).
        /// </summary>
        public static readonly RGBColor Orange = new RGBColor(255, 128, 0);
    }
}
