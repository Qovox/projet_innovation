﻿using System;

namespace Chroma.Library.Color
{
    /// <summary>
    /// Chroma HSV color.
    /// Based on code from Q42 API.
    /// </summary>
    public class HSVColor
    {
        /// <summary>
        /// Hue value.
        /// </summary>
        public int Hue { get; set; }

        /// <summary>
        /// Saturation value.
        /// </summary>
        public int Saturation { get; set; }

        /// <summary>
        /// Brightness value.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// HSV color constructor based on hue, saturation and brightness.
        /// </summary>
        /// <param name="hue">Hue value.</param>
        /// <param name="saturation">Saturation value.</param>
        /// <param name="value">Brightness value.</param>
        public HSVColor(int hue, int saturation, int value)
        {
            Hue = hue;
            Saturation = saturation;
            Value = value;
        }

        /// <summary>
        /// Converts this color to a RGB format.
        /// Based on code from Q42.HueApi.ColorConverters.HSB.HSB internal class.
        /// </summary>
        /// <returns>A RGBColor object representing this color.</returns>
        public RGBColor ToRGB()
        {
            var hue = (double) Hue;
            var saturation = (double) Saturation;
            var value = (double) Value;

            // Convert Hue into degrees for HSB.
            hue = hue / 182.04;
            // Bri and Sat must be values from 0-1 (~percentage).
            value = value / 255.0;
            saturation = saturation / 255.0;

            double r = 0;
            double g = 0;
            double b = 0;

            if (saturation == 0)
            {
                r = g = b = value;
            }
            else
            {
                // The color wheel consists of 6 sectors.
                double sectorPos = hue / 60.0;
                int sectorNumber = (int) (Math.Floor(sectorPos));
                // Get the fractional part of the sector.
                double fractionalSector = sectorPos - sectorNumber;

                // Calculate values for the three axes of the color. 
                double p = value * (1.0 - saturation);
                double q = value * (1.0 - (saturation * fractionalSector));
                double t = value * (1.0 - (saturation * (1 - fractionalSector)));

                // Assign the fractional colors to r, g, and b based on the sector the angle is in.
                switch (sectorNumber)
                {
                    case 0:
                        r = value;
                        g = t;
                        b = p;
                        break;
                    case 1:
                        r = q;
                        g = value;
                        b = p;
                        break;
                    case 2:
                        r = p;
                        g = value;
                        b = t;
                        break;
                    case 3:
                        r = p;
                        g = q;
                        b = value;
                        break;
                    case 4:
                        r = t;
                        g = p;
                        b = value;
                        break;
                    case 5:
                        r = value;
                        g = p;
                        b = q;
                        break;
                }
            }

            // Check if any value is out of byte range.
            if (r < 0)
            {
                r = 0;
            }
            if (g < 0)
            {
                g = 0;
            }
            if (b < 0)
            {
                b = 0;
            }

            return new RGBColor(r, g, b);
        }

        /// <summary>
        /// Converts this color as a six-digit hexadecimal string, in the form RRGGBB.
        /// </summary>
        /// <returns>A six-digit hexadecimal string representing this color.</returns>
        public string ToHex()
        {
            return ToRGB().ToHex();
        }
    }
}
