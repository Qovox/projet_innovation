﻿namespace Chroma.Library.WrappedObjects
{
    /// <summary>
    /// Chroma object wrapping Q42 API LocatedBridge object.
    /// </summary>
    public class LocatedBridge : Q42.HueApi.Models.Bridge.LocatedBridge
    {
        /// <summary>
        /// The bridge id.
        /// </summary>
        public new string BridgeId { get; set; }

        /// <summary>
        /// The ip address
        /// </summary>
        public new string IpAddress { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public LocatedBridge()
        {

        }

        /// <summary>
        /// Constructor based on Q42 object.
        /// </summary>
        /// <param name="locatedBridge">The Q42 object.</param>
        public LocatedBridge(Q42.HueApi.Models.Bridge.LocatedBridge locatedBridge)
        {
            BridgeId = locatedBridge.BridgeId;
            IpAddress = locatedBridge.IpAddress;
        }
    }
}
