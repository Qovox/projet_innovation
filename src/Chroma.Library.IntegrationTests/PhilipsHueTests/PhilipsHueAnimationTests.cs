﻿using Chroma.Library.Color;
using Chroma.Library.PhilipsHue;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chroma.Library.IntegrationTests.PhilipsHueTests
{
    [TestClass]
    public class PhilipsHueAnimationTests
    {
        private PhilipsHueClient client;
        private PhilipsHueAnimation animation;

        [TestInitialize]
        public void SetUp()
        {
            client = new PhilipsHueClient();
            client.InitializeAsync().GetAwaiter().GetResult();

            animation = new PhilipsHueAnimation();
            animation
                .SetColor(ChromaColors.Blue, lights: 1)
                .SetBrightness(0.5f, 0, lights: 1)
                .TurnOn(1600, lights: 1);
        }

        [TestMethod]
        public void TestTurnOnTurnOffAnimation()
        {
            Assert.IsTrue(animation.Frames.Count == 3);

            animation.TurnOff(0, lights: 1);
            Assert.IsTrue(animation.Frames.Count == 4);

            client.PlayAnimation(animation);
        }
    }
}
