﻿using Chroma.Library.PhilipsHue;
using Chroma.Library.Utils;
using Chroma.Library.WrappedObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Chroma.Library.IntegrationTests.PhilipsHueTests
{
    [TestClass]
    public class PhilipsHueLights
    {
        private PhilipsHueClient client;
        private Mock<PhilipsHueClient> mockedClient;
        private const string SettingsConfig = "settings.config";

        [TestInitialize]
        public void SetUp()
        {
            client = new PhilipsHueClient();
            mockedClient = new Mock<PhilipsHueClient>();
        }

        [TestMethod]
        public async Task TestSuccessfulInitialize()
        {
            Assert.IsTrue(await client.InitializeAsync());
            Assert.IsFalse(string.IsNullOrEmpty(client.LinkedBridge.BridgeId));
            Assert.IsFalse(string.IsNullOrEmpty(client.LinkedBridge.IpAddress));
            Assert.IsFalse(string.IsNullOrEmpty(client.Username));
            Assert.IsTrue(await client.CheckConnectionAsync());
        }

        [TestMethod]
        public async Task TestNoBridgesInitialize()
        {
            List<LocatedBridge> emptyList = new List<LocatedBridge>();

            mockedClient.Setup(c => c.ScanBridgesAsync()).Returns(Task.FromResult(emptyList));

            Assert.IsFalse(await mockedClient.Object.InitializeAsync());
            Assert.IsTrue(mockedClient.Object.LinkedBridge == null);
            Assert.IsTrue(string.IsNullOrEmpty(mockedClient.Object.Username));
        }

        [TestMethod]
        public async Task TestCheckConnection()
        {
            await client.InitializeAsync();
            Assert.IsTrue(await client.CheckConnectionAsync());
        }

        [TestMethod]
        public async Task TestSettingsFile()
        {
            await client.InitializeAsync();
            Assert.IsTrue(File.Exists(SettingsConfig) && new FileInfo(SettingsConfig).Length != 0);
            Settings Settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(SettingsConfig));
            Assert.IsNotNull(Settings.bridges);
        }

        [TestMethod]
        public async Task TestCacheTimer()
        {
            await client.InitializeAsync(cacheRefreshTime: 200);
            Assert.IsTrue(client.cacheTimer.Interval == 200);
        }
    }
}
