﻿using Chroma.Library.Color;
using Chroma.Library.PhilipsHue;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Chroma.Library.IntegrationTests.PhilipsHueTests.Subsystems
{
    [TestClass]
    public class PhilipsHueLightsTests
    {
        private PhilipsHueClient client;

        [TestInitialize]
        public async Task SetUp()
        {
            client = new PhilipsHueClient();
            await client.InitializeAsync();
        }

        private static bool AlmostEquals(double a, double b, double precision = float.Epsilon)
        {
            return Math.Abs(a - b) <= precision;
        }

        [TestMethod]
        public void TestGetLightRgbColor()
        {
            client.TurnOnLights(lights: 1);
            RGBColor rgbColor = ChromaColors.Olive;
            client.SetLightsColor(rgbColor, lights: 1);
            Thread.Sleep(105);
            RGBColor objectColor = client.GetLightRGBColor(1);
            Assert.IsTrue(AlmostEquals(rgbColor.R, objectColor.R, 1.0E-2));
            Assert.IsTrue(AlmostEquals(rgbColor.G, objectColor.G, 1.0E-2));
            Assert.IsTrue(AlmostEquals(rgbColor.B, objectColor.B, 1.0E-2));
            Assert.AreEqual(rgbColor.ToHex(), objectColor.ToHex());
        }

        [TestMethod]
        public void TestSetLightColor()
        {
            client.SetLightsColor(ChromaColors.Yellow, lights: 1);
        }

        [TestMethod]
        public void TestSetLightsBrightness()
        {
            client.TurnOnLights();
            client.SetLightsBrightness(0);
        }

        [TestMethod]
        public void TestGetLightBrightness()
        {
            client.TurnOnLights(lights: 1);
            client.SetLightsBrightness((float) 0.5, lights: 1);
            Thread.Sleep(105);
            Assert.IsTrue(AlmostEquals(client.GetLightBrightness(1), (float) 0.5, 1.0E-2));
        }

        [TestMethod]
        public void TestGetLightState()
        {
            client.TurnOnLights(lights: 2);
            Thread.Sleep(400);
            Assert.IsTrue(client.GetLightState(2));

            client.TurnOffLights(lights: 2);
            Thread.Sleep(400);
            Assert.IsFalse(client.GetLightState(2));
        }
    }
}
