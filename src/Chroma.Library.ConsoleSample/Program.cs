﻿using Chroma.Library.Color;
using Chroma.Library.PhilipsHue;
using Chroma.Library.PhilipsHue.Gaming;
using Chroma.Library.Utils;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Chroma.Library.ConsoleSample
{
    public class Program
    {
        private PhilipsHueClient Client { get; set; } = new PhilipsHueClient();
        private PhilipsHueAnimation animation = new PhilipsHueAnimation();
        private PhilipsHueAnimation animation2 = new PhilipsHueAnimation();

        public static async Task Main(string[] args)
        {
            Console.WriteLine("Initializes a new Philips Hue client.");
            Program consoleSample = new Program();

            Console.WriteLine("Initializes connection with the Philips Hue Bridge.");
            if (await consoleSample.Client.InitializeAsync())
            {
                // Setup
                consoleSample.Default();

                //consoleSample.BasicBehavior();
                //consoleSample.CacheBehavior();
                //consoleSample.BlinkFeature();
                //consoleSample.ColorLoopFeature();
                //consoleSample.TransitionTimeFeature();
                //consoleSample.GroupTransitionTimeFeature();
                //consoleSample.EventFeature();
                //consoleSample.AnimationFeature();
                //consoleSample.GetHSVColorFeature();
                //consoleSample.PlayMorseCodeAnimation();
                //consoleSample.PlayEndGameAnimation();
                //consoleSample.PlayPresetOneEndGameAnimation();
                //consoleSample.PlayPresetTwoEndGameAnimation();
                //consoleSample.PlayPresetThreeEndGameAnimation();
                //consoleSample.PlayExplosion();

                // Cleanup
                consoleSample.Default();

                Console.ReadLine();
            }
            else
            {
                Console.WriteLine(PhilipsHueClient.LastExceptionDetails);
            }
        }

        public void Default()
        {
            Client.TurnOnLights();
            Thread.Sleep(100);
            Client.SetLightsColor(ChromaColors.White);
            Thread.Sleep(100);
            Client.TurnOffLights();
            Thread.Sleep(400);
        }

        public void BasicBehavior()
        {
            Console.WriteLine("Turns on the light with id 1.");
            Client.TurnOnLights(lights: 1);
            Thread.Sleep(400);

            Console.WriteLine("Sets the color of the light with id 1 to blue.");
            Client.SetLightsColor(ChromaColors.Blue, lights: 1);
            Thread.Sleep(400);

            Console.WriteLine("Turns off the light with id 1.");
            Client.TurnOffLights(lights: 1);
            Thread.Sleep(400);
        }

        public void CacheBehavior()
        {
            Console.WriteLine("Turns on the lights 1 and 2.");
            Client.TurnOnLights(lights: new int[] { 1, 2 });
            Thread.Sleep(400);

            Console.WriteLine("Set the color of the lights with id 1 and to 2 to magenta.");
            Client.SetLightsColor(ChromaColors.Magenta, lights: new int[] { 1, 2 });
            Thread.Sleep(400);

            Console.WriteLine("Turns off the lights with id 1 and 2 to ensure the light are turned off before calling SetColor on them.");
            Client.TurnOffLights(lights: new int[] { 1, 2 });
            Thread.Sleep(400);

            Console.WriteLine("Sets the color of the lights with id 1 and 2 (in cache) to blue.");
            Client.SetLightsColor(ChromaColors.Blue, lights: new int[] { 1, 2 });
            Thread.Sleep(100);

            Console.WriteLine("Turns on the lights with id 1 and 2 (They should appear as blue!).");
            Client.TurnOnLights(lights: new int[] { 1, 2 });
            Thread.Sleep(1000);

            Console.WriteLine("Turns off the light with id 1 and 2.");
            Client.TurnOffLights(lights: new int[] { 1, 2 });
            Thread.Sleep(400);
        }

        public void BlinkFeature()
        {
            Console.WriteLine("Blinks each light once.");
            Client.BlinkLights(BlinkMode.Once);
            Thread.Sleep(400);

            Console.WriteLine("Set the color of the light 1 to yellow.");
            Client.SetLightsColor(ChromaColors.Yellow, lights: 1);

            Console.WriteLine("Set the color of the light 3 to cyan.");
            Client.SetLightsColor(ChromaColors.Blue, lights: 3);

            Thread.Sleep(100);

            Console.WriteLine("Turns on the lights with id 1 and 3.");
            Client.TurnOnLights(lights: new int[] { 1, 3 });
            Thread.Sleep(400);

            Console.WriteLine("Blinks each light multiple times. Waits 7 and a half seconds before stopping the blink.");
            Client.BlinkLights(BlinkMode.Multiple);
            Thread.Sleep(7500);

            Console.WriteLine("Stopping the blinking for each light.");
            Client.StopBlinkingLights();
            Thread.Sleep(400);

            Console.WriteLine("Turns off each light.");
            Client.TurnOffLights();
            Thread.Sleep(400);
        }

        public void ColorLoopFeature()
        {
            Console.WriteLine("Sets the color of the light 1 to maroon.");
            Client.SetLightsColor(ChromaColors.Maroon, lights: 1);
            Thread.Sleep(100);

            Console.WriteLine("Turns on light with id 1.");
            Client.TurnOnLights(lights: 1);
            Thread.Sleep(400);

            Console.WriteLine("Loops on light with id 1.");
            Client.ColorLoopOnLights(1);
            Thread.Sleep(10000);

            Console.WriteLine("Stops looping on light with id 1.");
            Client.StopColorLoopingOnLights(1);
            Thread.Sleep(400);

            Console.WriteLine("Turns off light with id 1.");
            Client.TurnOffLights(lights: 1);
            Thread.Sleep(400);
        }

        public void TransitionTimeFeature()
        {
            Console.WriteLine("Sets the color of the light with id 1 to red.");
            Client.SetLightsColor(ChromaColors.Red, lights: 1);
            Thread.Sleep(100);

            Console.WriteLine("Turns on light with id 1 in 1 second.");
            Client.TurnOnLights(7500, lights: 1);
            Thread.Sleep(7500);

            Console.WriteLine("Sets the color of the light with id 1 to blue in 7 seconds and a half.");
            Client.SetLightsColor(ChromaColors.Blue, 7500, 1);
            Thread.Sleep(7500);

            Console.WriteLine("Instantly turns off the light with id 1.");
            Client.TurnOffLights(0, lights: 1);
        }

        public void GroupTransitionTimeFeature()
        {
            Console.WriteLine("Sets the color of the lights to red.");
            Client.SetLightsColor(ChromaColors.Red);
            Thread.Sleep(100);

            Console.WriteLine("Turns on all the lights in 1 second.");
            Client.TurnOnLights(7500);
            Thread.Sleep(7500);

            Console.WriteLine("Sets the color of each light to blue in 7 seconds and a half.");
            Client.SetLightsColor(ChromaColors.Blue, 7500);
            Thread.Sleep(7500);

            Console.WriteLine("Instantly turns off all the lights.");
            Client.TurnOffLights(0);
        }

        public void EventFeature()
        {
            Console.WriteLine("Sets the color of the lights with id 1 and 2 to gray.");
            Client.SetLightsColor(ChromaColors.Gray, lights: new int[] { 1, 2 });
            Thread.Sleep(100);

            Console.WriteLine("Turns on light with id 1.");
            Client.TurnOnLights(lights: new int[] { 1, 2 });
            Thread.Sleep(400);

            Console.WriteLine("Subscribing to the event...");
            var eventRaised = new PhilipsHueClient.LightChangeEventHandler(OnEventRaisedFeature);

            Client.StartCacheAutoUpdate();
            Client.LightChangeEvent += eventRaised;

            Console.WriteLine("Sets the color of the lights with id 1 and 2 to blue in 5 seconds.");
            Client.SetLightsColor(ChromaColors.Blue, 5000, new int[] { 1, 2 });
            Thread.Sleep(5000);

            Console.WriteLine("Turns off the lights with id 1 and 2 in 1 second.");
            Client.TurnOffLights(lights: new int[] { 1, 2 });
            Thread.Sleep(1000);

            Console.WriteLine("Unsubscribe from event");
            Client.StopCacheAutoUpdate();
            Client.LightChangeEvent -= eventRaised;
        }

        private void OnEventRaisedFeature(PhilipsHueClient.LightChangeEventArgs e)
        {
            Console.WriteLine("Light id event raised: " + e._lightId);
            Console.WriteLine("Light id color update: " + e._HexColor + "\n");
        }

        public void AnimationFeature(int loop = 1)
        {
            // HeartBeat animation
            animation
                .TurnOff(5000, 1)
                .SetColor(ChromaColors.Cyan, lights: 1)
                .TurnOn(5000, 1)
                .SetBrightness(0, 2500, 1)
                .Wait(5000)
                .TurnOff(1000, 1);

            animation2
                .TurnOff(5000, 2)
                .SetColor(ChromaColors.Magenta, lights: 2)
                .TurnOn(5000, 2)
                .SetBrightness(0, 2500, 2)
                .Wait(5000)
                .TurnOff(1000, 2);

            Console.WriteLine("Playing animation...");
            Client.PlayAnimation(animation, loop);

            Console.WriteLine("Playing animation2...");
            Client.PlayAnimation(animation2, loop);
        }

        public void GetHSVColorFeature()
        {
            Console.WriteLine("Turns on light with id 1.");
            Client.TurnOnLights(lights: 1);
            Thread.Sleep(400);

            Console.WriteLine("Sets the color of the light with id 1.");
            Client.SetLightsColor(new HSVColor(45000, 255, 255), lights: 1);
            Thread.Sleep(800);

            Console.WriteLine("Retrieves the HSV color previously set");
            HSVColor color = Client.GetLightHSVColor(1);
            Console.WriteLine("HSV color from light 1: {0} {1} {2}", color.Hue, color.Saturation, color.Value);
            Thread.Sleep(400);

            Console.WriteLine("Turns off the light with id 1.");
            Client.TurnOffLights(lights: 1);
        }

        public void GetBrightnessFeature()
        {
            Console.WriteLine("Turns on the light with id 1.");
            Client.TurnOnLights(lights: 1);
            Thread.Sleep(400);

            Console.WriteLine("Sets the color of the light with id 1 to blue.");
            Client.SetLightsColor(ChromaColors.Blue, lights: 1);
            Thread.Sleep(400);

            Console.WriteLine("Gets the brightness of the light with id 1");
            Console.WriteLine("Brightness of light 1: {0}", Client.GetLightBrightness(1));

            Console.WriteLine("Turns off the light with id 1.");
            Client.TurnOffLights(lights: 1);
            Thread.Sleep(400);
        }

        public void InstantChangesOnDifferentLights()
        {
            Console.WriteLine("Turns on each light.");
            Client.TurnOnLights();
            Thread.Sleep(400);

            Console.WriteLine("Sets the color of the light with id 1 to blue.");
            Client.SetLightsColor(ChromaColors.Blue, lights: 1);

            Console.WriteLine("Sets the color of the light with id 2 to magenta.");
            Client.SetLightsColor(ChromaColors.Magenta, lights: 2);

            Console.WriteLine("Sets the color of the light with id 3 to yellow.");
            Client.SetLightsColor(ChromaColors.Yellow, lights: 3);

            Thread.Sleep(2500);

            Console.WriteLine("Turns off each light.");
            Client.TurnOffLights();
            Thread.Sleep(400);
        }

        #region Gaming

        public void PlayMorseCodeAnimation()
        {
            Console.WriteLine("Building Morse Animation...");
            MorseCodeAnimation morseCodeAnimation = new MorseCodeAnimation("sos", color: ChromaColors.Blue, lights: 1);

            Console.WriteLine("Playing Morse Animation...");
            Client.PlayAnimation(morseCodeAnimation, 3);
        }

        public void PlayExplosion()
        {
            Console.WriteLine("Building Explosion Animation...");
            ExplosionAnimation explosion = new ExplosionAnimation(Preset.Two);

            Console.WriteLine("Playing Explosion Animation...");
            Client.PlayAnimation(explosion);
            Thread.Sleep(7500);
        }

        public void PlayEndGameAnimation()
        {
            Console.WriteLine("Building victory animation...");
            EndGameAnimation victoryAnimation = new EndGameAnimation(ChromaColors.Magenta);

            Console.WriteLine("Playing victory animation...");
            Client.PlayAnimation(victoryAnimation);
            Thread.Sleep(7500);
        }

        public void PlayPresetOneEndGameAnimation()
        {
            Console.WriteLine("Building victory animation...");
            EndGameAnimation victoryAnimation = new EndGameAnimation(Preset.One);

            Console.WriteLine("Playing preset one of victory animation...");
            Client.PlayAnimation(victoryAnimation);
            Thread.Sleep(15000);
        }

        public void PlayPresetTwoEndGameAnimation()
        {
            Console.WriteLine("Building victory animation...");
            EndGameAnimation victoryAnimation = new EndGameAnimation(Preset.Two);

            Console.WriteLine("Playing preset two of victory animation...");
            Client.PlayAnimation(victoryAnimation);
            Thread.Sleep(10000);
        }

        public void PlayPresetThreeEndGameAnimation()
        {
            Console.WriteLine("Building victory animation...");
            EndGameAnimation victoryAnimation = new EndGameAnimation(Preset.Three);

            Console.WriteLine("Playing preset three of victory animation...");
            Client.PlayAnimation(victoryAnimation);
            Thread.Sleep(10000);
        }

        #endregion
    }
}
