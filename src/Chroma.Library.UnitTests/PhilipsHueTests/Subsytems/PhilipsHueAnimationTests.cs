﻿using Chroma.Library.PhilipsHue;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Chroma.Library.UnitTests.PhilipsHueTests.Subsytems
{
    [TestClass]
    public class PhilipsHueAnimationTests
    {
        private PhilipsHueAnimation animation;

        [TestInitialize]
        public void SetUp()
        {
            animation = new PhilipsHueAnimation();
            animation.TurnOn().TurnOff(lights: new int[] { 1, 2 });
        }

        [TestCleanup]
        public void CleanUp()
        {
            animation = new PhilipsHueAnimation();
        }

        [TestMethod]
        public void TestReset()
        {
            animation.IsStopped = false;
            animation.RemoveLight(1);
            animation.Reset();

            Assert.IsTrue(animation.IsStopped);
            Assert.IsFalse(animation.IsModified);
            Assert.AreEqual(animation.Lights.ToString(), animation.ModifiedLights.ToString());
        }

        [TestMethod]
        public void TestUpdateLights()
        {
            animation.UpdateLights(3);

            Assert.IsTrue(animation.Lights.Contains(3));
            Assert.IsTrue(animation.ModifiedLights.Contains(3));
        }

        [TestMethod]
        public void TestRemoveLight()
        {
            animation.RemoveLight(1);

            Assert.IsTrue(animation.IsModified);
            Assert.IsTrue(animation.Lights.Count >= animation.ModifiedLights.Count);

            foreach (AnimationFrame frame in animation.Frames)
            {
                Assert.IsTrue(frame.IsModified);
                Assert.IsTrue(frame.Lights.ToList().Count >= frame.ModifiedLights.Count);
            }
        }

        [TestMethod]
        public void TestRemoveLightReset()
        {
            animation.RemoveLight(1);
            animation.RemoveLight(2);

            Assert.IsFalse(animation.ModifiedLights.Count == 0);

            foreach (AnimationFrame frame in animation.Frames)
            {
                Assert.IsFalse(frame.Lights.Count() != frame.ModifiedLights.Count);
            }
        }

        [TestMethod]
        public void TestFrameReset()
        {
            AnimationFrame frame = new TurnOnFrame(400, 1, 2);
            frame.ModifiedLights.Remove(1);
            frame.IsModified = true;
            frame.Reset();

            Assert.IsFalse(frame.IsModified);
            Assert.AreEqual(frame.Lights.ToList().ToString(), frame.ModifiedLights.ToString());
        }
    }
}
