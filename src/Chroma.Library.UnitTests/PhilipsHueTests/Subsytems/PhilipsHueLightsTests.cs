﻿using Chroma.Library.Color;
using Chroma.Library.PhilipsHue.Subsystems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Q42.HueApi;
using Q42.HueApi.Interfaces;
using System.Threading.Tasks;

namespace Chroma.Library.UnitTests.PhilipsHueTests.Subsystems
{
    [TestClass]
    public class PhilipsHueLightsTests
    {
        private Mock<ILocalHueClient> client = new Mock<ILocalHueClient>();
        private PhilipsHueLights subsytem;
        private Light light;
        private State state;

        #region Test SetUp

        [TestInitialize]
        public void SetUp()
        {
            light = new Light();
            state = new State
            {
                On = true,
                Hue = 45000,
                Saturation = 255,
                Brightness = 128
            };
            light.State = state;

            client.Setup(c => c.GetLightAsync(It.IsAny<string>())).Returns(Task.FromResult(light));
            subsytem = new PhilipsHueLights(client.Object);
        }

        #endregion

        #region Test Get Brightness

        [TestMethod]
        public void TestGetLightBrightness()
        {
            float expected = (float) (128 / 255.0);
            Assert.AreEqual(expected, subsytem.GetLightBrightness(1));
        }

        #endregion

        #region Test Get Colors

        [TestMethod]
        public void TestGetLightRGBColor()
        {
            RGBColor expected = (new HSVColor(state.Hue ?? 0, state.Saturation ?? 0, state.Brightness)).ToRGB();
            RGBColor actual = subsytem.GetLightRGBColor(1);
            Assert.AreEqual(expected.R, actual.R);
            Assert.AreEqual(expected.G, actual.G);
            Assert.AreEqual(expected.B, actual.B);
        }

        [TestMethod]
        public void TestGetLightHSVColor()
        {
            HSVColor expected = new HSVColor(state.Hue ?? 0, state.Saturation ?? 0, state.Brightness);
            HSVColor actual = subsytem.GetLightHSVColor(1);
            Assert.AreEqual(expected.Hue, actual.Hue);
            Assert.AreEqual(expected.Saturation, actual.Saturation);
            Assert.AreEqual(expected.Value, actual.Value);
        }

        [TestMethod]
        public void TestGetLightHexColor()
        {
            string expected = new HSVColor(state.Hue ?? 0, state.Saturation ?? 0, state.Brightness).ToHex();
            Assert.AreEqual(expected, subsytem.GetLightHexColor(1));
        }

        #endregion

        #region Test Get Light State

        [TestMethod]
        public void TestGetLightState()
        {
            bool expected = state.On;
            Assert.AreEqual(expected, subsytem.GetLightState(1));
        }

        #endregion

    }
}
