﻿using Chroma.Library.PhilipsHue;
using Chroma.Library.PhilipsHue.Gaming;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chroma.Library.UnitTests.PhilipsHueTests.Gaming
{
    [TestClass]
    public class MorseCodeAnimationTests
    {
        private MorseCodeAnimation morse;

        [TestInitialize]
        public void SetUp()
        {
            morse = new MorseCodeAnimation("sos", lights: 1);
        }

        [TestMethod]
        public void TestNumberOfFrames()
        {
            PhilipsHueAnimation Animation = morse.Animation as PhilipsHueAnimation;
            Assert.AreEqual(44, Animation.Frames.Count);
        }
    }
}
