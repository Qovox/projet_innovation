﻿using Chroma.Library.PhilipsHue;
using Chroma.Library.PhilipsHue.Gaming;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chroma.Library.UnitTests.PhilipsHueTests.Gaming
{
    [TestClass]
    public class ExplosionAnimationTests
    {
        private ExplosionAnimation explosion;

        [TestInitialize]
        public void SetUp()
        {
            explosion = new ExplosionAnimation(new int[] { 1 });
        }

        [TestMethod]
        public void TestNumberOfFrames()
        {
            PhilipsHueAnimation Animation = explosion.Animation as PhilipsHueAnimation;
            Assert.AreEqual(11, Animation.Frames.Count);
        }
    }
}
