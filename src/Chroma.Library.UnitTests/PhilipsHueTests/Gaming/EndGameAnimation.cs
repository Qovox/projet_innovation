﻿using Chroma.Library.PhilipsHue;
using Chroma.Library.PhilipsHue.Gaming;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Chroma.Library.UnitTests.PhilipsHueTests.Gaming
{
    [TestClass]
    public class EndGameAnimationTests
    {
        private EndGameAnimation animation;

        [TestInitialize]
        public void SetUp()
        {
            animation = new EndGameAnimation(lights: 1);
        }

        [TestMethod]
        public void TestNumberOfFrames()
        {
            PhilipsHueAnimation Animation = animation.Animation as PhilipsHueAnimation;
            Assert.AreEqual(14, Animation.Frames.Count);
        }
    }
}
