﻿using Chroma.Library.PhilipsHue;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace Chroma.Library.UnitTests.PhilipsHueTests
{
    [TestClass]
    public class PhilipsHueClientAnimationTests
    {
        private PhilipsHueClient client;
        private PhilipsHueAnimation animation;

        [TestInitialize]
        public void SetUp()
        {
            client = new PhilipsHueClient();
            animation = new PhilipsHueAnimation();
            animation.TurnOn().TurnOff(lights: new int[] { 1, 2 });
        }

        [TestMethod]
        public void TestStopAnimation()
        {
            Mock<AnimationFrame> mock = new Mock<AnimationFrame>();
            mock.Setup(af => af.Execute(It.IsAny<PhilipsHueClient>()));
            client.PlayAnimation(animation);
            client.StopAnimation(1);

            Assert.IsFalse(animation.ModifiedLights.Contains(1));
            Assert.IsFalse(animation.IsStopped);
        }

        [TestMethod]
        public void TestCompleteStopAnimation()
        {
            Mock<AnimationFrame> mock = new Mock<AnimationFrame>();
            mock.Setup(af => af.Execute(It.IsAny<PhilipsHueClient>()));
            client.PlayAnimation(animation);
            client.StopAnimation(1, 2);

            Assert.IsTrue(animation.IsStopped);
            Assert.AreEqual(animation.Lights.ToList().ToString(), animation.ModifiedLights.ToString());
        }
    }
}
