﻿using Chroma.Library.Utils;
using Chroma.Library.WrappedObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace Chroma.Library.UnitTests.Utils
{
    [TestClass]
    public class ChromaAppDataTests
    {
        private string SettingsPath = "";
        private const string SettingsFile = "settings.config";

        [TestCleanup]
        public void Cleanup()
        {
            File.Delete(SettingsPath + SettingsFile);
            if (!string.IsNullOrEmpty(SettingsPath))
            {
                Directory.Delete(SettingsPath);
            }
        }

        [TestMethod]
        public void TestFetchSettingsWithoutData()
        {
            ChromaAppData.FetchSettings();
            ChromaAppData.SaveSettings();
            // If the GetBridgeSettings returns something, here null, then a Settings object has been initialized.
            Assert.AreEqual(null, ChromaAppData.GetBridgeSettings(new LocatedBridge()));
        }

        [TestMethod]
        public void TestFetchSettingsWithoutPath()
        {
            ChromaAppData.FetchSettings();
            ChromaAppData.SaveSettings();
            ChromaAppData.FetchSettings();
            // If the GetBridgeSettings returns something, here null, then a Settings object has been fetched.
            Assert.AreEqual(null, ChromaAppData.GetBridgeSettings(new LocatedBridge()));
        }

        [TestMethod]
        public void TestFetchSettingsWithPathAndAddData()
        {
            SettingsPath = "test/";
            ChromaAppData.FetchSettings(SettingsPath);

            BridgeSettings bs = new BridgeSettings()
            {
                appkey = "test",
                properties = new LocatedBridge()
                {
                    BridgeId = "testId",
                    IpAddress = "testIp"
                }
            };

            ChromaAppData.AddBridgeSettings(bs);
            ChromaAppData.SaveSettings(SettingsPath);
            ChromaAppData.FetchSettings(SettingsPath);

            // If the GetBridgeSettings returns something, here null, then a Settings object has been either fetched or initialized.
            Assert.AreEqual(null, ChromaAppData.GetBridgeSettings(new LocatedBridge()));

            BridgeSettings retrievedBridgeSettings = ChromaAppData.GetBridgeSettings(bs.properties);

            Assert.AreEqual(bs.properties.BridgeId, retrievedBridgeSettings.properties.BridgeId);
            Assert.AreEqual(bs.properties.IpAddress, retrievedBridgeSettings.properties.IpAddress);
            Assert.AreEqual(bs.appkey, retrievedBridgeSettings.appkey);
        }

        [TestMethod]
        public void TestSaveSettingsWithoutPath()
        {
            ChromaAppData.SaveSettings();
            Assert.IsTrue(File.Exists(SettingsFile));
        }

        [TestMethod]
        public void TestSaveSettingsWithPath()
        {
            SettingsPath = "test/";
            ChromaAppData.SaveSettings(SettingsPath);
            Assert.IsTrue(File.Exists(SettingsPath + SettingsFile));
        }
    }
}
